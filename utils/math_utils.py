import numpy as np
from numba import jit

@jit(nopython=True)
def normalize(obj):
    
    return obj / np.linalg.norm(obj)

@jit(nopython=True)
def skew_symm(vec):
        
    return np.array([[0., vec[2], -vec[1]],[-vec[2], 0., vec[0]],[vec[1], -vec[0], 0.]])

@jit(nopython=True)
def cross_product(a, b):
    
    return np.array([a[1]*b[2]-a[2]*b[1], a[2]*b[0]-a[0]*b[2], a[0]*b[1] - a[1]*b[0]])

def quat_product(q1, q2):
    
    q = np.zeros(4)
    q[0] = q1[0] * q2[0] - q1[1:].dot(q2[1:])
    q[1:] = q1[0] * q2[1:] + q2[0] * q1[1:] + cross_product(q1[1:], q2[1:])
    
    return q

@jit(nopython=True)
def rotate_vec_with_quat(q, vec):
    
    q = quat_conjugate(q)
    
    qxvec = cross_product(q[1:], vec)
    
    return q[1:].dot(vec) * q[1:] + q[0]**2. * vec + 2. * q[0] * qxvec + cross_product(q[1:], qxvec)

def quat2rpy(quat):
    
    q0, q1, q2, q3 = quat
    
    roll = np.arctan2(2. * (q0 * q1 + q2 * q3), 1. - 2. * (q1**2 + q2**2))
    pitch = np.arcsin(2. * (q0 * q2 - q1 * q3))
    yaw = np.arctan2(2. * (q0 * q3 + q1 * q2), 1. - 2. * (q2**2 + q3**2))
    
    return np.vstack((roll, pitch, yaw))

@jit(nopython=True)
def quat_conjugate(q):
    
    q_new = np.copy(q)
    q_new[1:] *= -1.
    
    return q_new

def ndarray_to_nx1(vec):
    
    return np.array([vec]).T

def nx1_to_ndarray(vec):
    
    return vec.T[0]