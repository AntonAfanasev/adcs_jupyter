import numpy as np
from matplotlib import pyplot as plt
from itertools import compress
from warnings import warn
from os.path import exists

from math_utils import quat2rpy

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Avant Garde']})
rc('text', usetex=True)

def time_axis(ax, unit):
    
    if unit == 'seconds':
        pass
    elif unit == 'minutes':
        ax /= 60.
    elif unit == 'hours':
        ax /= 3600.
    elif unit == 'days':
        ax /= 86400.
    else:
        raise Exception("No such time measurement unit in my code [seconds, minutes, hours, days]")
        
def time_label(t1, t2, u_list, unit):
    
    if unit == 'seconds':
        return '(%5.1i, %5.1i) %s$$' % (t1, t2, u_list[0])
    elif unit == 'minutes':
        return '(%5.1f, %5.1f) %s$$' % (t1 / 60., t2 / 60., u_list[1])
    elif unit == 'hours':
        return '(%5.2f, %5.2f) %s$$' % (t1 / 3600., t2 / 3600., u_list[2])
    elif unit == 'days':
        return '(%5.3f, %5.3f) %s$$' % (t1 / 86400., t2 / 86400., u_list[3])
    else:
        raise Exception("No such time measurement unit in my code [seconds, minutes, hours, days]")
        
def angle_axis(unit):
    
    if unit == 'radians':
        mul = 1.
    elif unit == 'degrees':
        mul = 180. / np.pi
    else:
        raise Exception("No such angle measurement unit in my code [radians, degrees]")
        
    return mul

def if_file_exists(file_name, ending):
    
    while exists(file_name + ending):
        if file_name[-7:] == '_Copy00':
            raise Exception('Too much files with the name {} in directory'.format(file_name[:-7]))
        if file_name[-7:-2] == '_Copy':
            file_name = file_name[:-1] + str((int(file_name[-1]) + 1) % 10)
            if file_name[-1] == '0':
                file_name = file_name[:-2] + str((int(file_name[-2]) + 1) % 10) + file_name[-1]
        else:
            file_name += '_Copy01'
            
    return file_name

def print_filter(N, X, q0, Omega, T1, T5, J, Kw, Ka, alt, incl, m_MAX, P_0, sigma_M, B_bias, sigma_meas,
                 storm_magn, sigma_B_actual, storm_interval, structure=[1,0,1,0,1,0,1,0], AAV_limits=None,
                 RAV_limits=None, RPY_limits=None, QUAT_limits=None, add_q0=True, time_unit='hours',
                 angle_unit='radians', title=True, units_in_title=True):
    
    num_of_plots = np.sum(structure)
    if not num_of_plots:
        raise Exception('No plots chosen')
        
    dynamics = (sigma_M == 0) and (B_bias == 0) and (sigma_meas == 0) and ((storm_magn == 0) or (storm_magn is None))
        
    inds = list(compress(range(len(structure)), structure))
    
    bea = [0,] * num_of_plots
    
    time = np.linspace(0, (N-1)*(T1+T5), N)
    time_axis(time, time_unit)
        
    angle_mul = angle_axis(angle_unit)
    
    roll, pitch, yaw = quat2rpy(q0, X[0], X[1], X[2])
    
    #-------------Aggregation-----------------------------------------------------------------------------------------------
    
    X_cmp = [X[3:] * angle_mul, Omega * angle_mul, np.vstack((roll, pitch, yaw)) * angle_mul, np.vstack((X[:3], q0))]
    
    labels_AAV = ['$\omega_1$', '$\omega_2$', '$\omega_3$']
    labels_RAV = ['$\Omega_1$', '$\Omega_2$', '$\Omega_3$']
    labels_RPY = ['Roll', 'Pitch', 'Yaw']
    labels_QUAT = ['$q_1$', '$q_2$', '$q_3$', '$q_0$']
    
    labels = [labels_AAV, labels_RAV, labels_RPY, labels_QUAT]
    
    titles = ['absolute angular velocity', 'relative angular velocity', 'orientation', 'quaternion components']
    
    y_labels = ['{}/s'.format(angle_unit[:3]), '{}/s'.format(angle_unit[:3]), angle_unit, 'units']
    
    limits = [AAV_limits, RAV_limits, RPY_limits, QUAT_limits]
    
    #-----Adjusting subplots and choosing their type-------------------------------------------------------------------------
    
    if dynamics:
        adj_arr = [0.61, 0.7403, 0.803, 0.8398, 0.8625, 0.8798, 0.8927, 0.9016]
        subpl_shift = 2.41
        
        name_type = 'dynamics'
        title_string = 'Dynamics'
    else:
        adj_arr = [0.5, 0.643, 0.721, 0.77, 0.803, 0.827, 0.8455, 0.86]
        subpl_shift = 4
        
        name_type = 'filter'
        title_string = 'Filter results'
        
    #------Creating subplots-----------------------------------------------------------------------------------------------
    
    f, ax = plt.subplots(num_of_plots, 1, sharex=True, figsize=(12, num_of_plots * 3 + subpl_shift), dpi=300, squeeze=False)
    
    f.subplots_adjust(top = adj_arr[num_of_plots-1])
    
    if (not title) and (units_in_title): warn('Can\'t put units in non-existing title')
    
    if title:
        
        #-----------Units in title-------------------------------------------------------------------------------------------
        
        if units_in_title:
            radps = '\\left[ {\\large \\textsf{rad}} / {\\large \\textsf{s}} \\right]'
            Am2 = '\\left[ {\\large \\textsf{A}}\!\cdot\! \
                   {\\large \\textsf{m}}^{\\small \\textsf{2}} \\right]'
            kgm2 = '\\left[ {\\large \\textsf{kg}}\!\cdot\! \
                    {\\large \\textsf{m}}^{\\small \\textsf{2}} \\right]'
            NmpT2 = '\\left[ {\\large \\textsf{N}}\!\cdot\!{\\large \\textsf{m}} / {\\large \\textsf{T}}^\
                    {\\small \\textsf{ 2}} \\right]'
            km = '\\left[ {\\large \\textsf{km}} \\right]'
            sec = '\\left[ {\\large \\textsf{s}} \\right]'
            P_unit = '\\left[ {\\large \\textsf{rad}}^{\\small \\textsf{2}} (\!\\times 3), {\\large \\textsf{rad}}^{\\small \
                      \\textsf{2}} / {\\large \\textsf{s}}^{\\small \\textsf{2}} (\!\\times 3) \\right]'
            hour = '\\left[ {\\large \\textsf{h}} \\right]'
            Nm = '\\left[ {\\large \\textsf{N}}\!\cdot\!{\\large \\textsf{m}} \\right]'
            tesla = '\\left[ {\\large \\textsf{T}} \\right]'
            minute = '\\left[ {\\large \\textsf{min}} \\right]'
            day = '\\left[ {\\large \\textsf{days}} \\right]'
        else:
            radps = Am2 = kgm2 = NmpT2 = km = sec = P_unit = hour = Nm = tesla = minute = day = ''
        st_int_unit = [sec, minute, hour, day]
        
        #-----Different types of titles------------------------------------------------------------------------------------
        
        if dynamics:
            
            #-----No noise in title 'Dynamics'-----------------------------------------------------------------------------
            
            noise_string = ''
            title_type = '{\\huge \\textsc{%s}}' % (title_string)
            
        else:
            
            #-----Title 'Filter results'-----------------------------------------------------------------------------------
            
            title_type = '{\\huge \\textsc{%s}}' % (title_string)
            
            #-----Initial covariance matrix in title-----------------------------------------------------------------------
            
            P_init_string = '$$P^{{\\normalsize \\textsf{ init}}} = {\\Large \\textsf{diag}} (%5.2f, %5.2f, %5.2f, \
                             %5.2f, %5.2f, %5.2f) %s$$' % (P_0[0,0], P_0[1,1], P_0[2,2], P_0[3,3], P_0[4,4], P_0[5,5], P_unit)
        
            #-----Internal torque noise in title---------------------------------------------------------------------------

            if sigma_M == 0:
                torq_noise_string = '{\\large \\textsf{No internal torque noise in the model}}'
            else:
                torq_noise_string = '\\eta_{{\\normalsize \\textsf{ torque}}} \\sim \\mathcal{N} \\left(0, \{%1.0e\}^2 \
                                     \\right) %s' % (sigma_M, Nm)

            #------Magnetometer noise in title----------------------------------------------------------------------------

            if (B_bias != 0) or (sigma_meas != 0):
                if sigma_meas == 0:
                    meas_noise_string = '\\eta_{{\\normalsize \\textsf{ magnetometer}}} \\sim \\delta \\left( %1.0e \\right) \
                                         %s' % (B_bias, tesla)
                else:
                    if B_bias != 0:
                        B_bias_string = '%1.0e' % B_bias
                    else:
                        B_bias_string = '0'
                    meas_noise_string = '\\eta_{{\\normalsize \\textsf{ magnetometer}}} \\sim \\mathcal{N} \\left(' +\
                                        B_bias_string + ', \{%1.0e\}^2 \\right) %s' % (sigma_meas, tesla)
            else:
                meas_noise_string = '{\\large \\textsf{No measurements\' noise in the model}}'

            #-----Magnetic storm in title---------------------------------------------------------------------------------

            if isinstance(storm_interval, list) or isinstance(storm_interval, tuple):
                if len(storm_interval) == 2:
                    st_int_string = time_label(storm_interval[0], storm_interval[1], st_int_unit, time_unit)

                    storm_string = '$$\\eta_{{\\normalsize \\textsf{ storm}}} \\sim \\mathcal{N} \\left(%1.0e, \{%1.0e\}^2 \
                    \\right) %s \qquad \\Delta T_{{\\normalsize \\textsf{ storm}}} = '\
                    % (storm_magn, sigma_B_actual, tesla) + st_int_string
                else:
                    raise Exception('Interval of magnetic storm is in invalid form (not 2 entries)')
            else:
                if (storm_magn is None) or (storm_magn == 0):
                    storm_string = '{\\large \\textsf{No magnetic storm in the model}}'
                else:
                    raise Exception('No interval is given for magnetic storm')
            
            #-----Overall noise in title----------------------------------------------------------------------------------
            
            noise_string = '\n' + P_init_string + '\n$$' + torq_noise_string + '\qquad' + meas_noise_string + '$$\n' + \
                           storm_string
        
        sup=f.suptitle('%s\n$$q^{{\\normalsize \\textsf{init}}} = (%1.1f, %1.1f, %1.1f, %1.1f) \
                       \qquad \omega^{{\\normalsize \\textsf{init}}} = (%5.1f, %5.1f, %5.1f) %s$$\n$$J = {\\Large \
                       \\textsf{diag}}(%5.3f, %5.3f, %5.3f) %s \qquad (k\'_\omega, k_a) = (%5.1i, %5.1i) %s \qquad \
                       \\mu_{\\max} = %5.1f %s$$\n$$h_{{\\normalsize \\textsf{orb}}} = %5.1i %s \qquad i = %5.1f^{\\circ} \
                       \qquad (T_1, T_5) = (%5.1f, %5.1f) %s$$'
                       % (title_type, q0[0], X[0,0], X[1,0], X[2,0], X[3,0], X[4,0], X[5,0], radps, J[0,0], J[1,1], J[2,2], \
                          kgm2, int(Kw), int(Ka), NmpT2, m_MAX, Am2, alt / 1000, km, incl * 180 / np.pi, T1, T5, sec) + \
                          noise_string, 
                          fontsize=18, linespacing = 1.7)
    
        bea += [sup,]
        
    ax_num = 0
        
    for i in inds:
        
        for j in range(3):
            
            ax[ax_num, 0].plot(time, X_cmp[i/2][j], label=labels[i/2][j])
            
        if (i/2 == 3) and add_q0:
            
            ax[ax_num, 0].plot(time, X_cmp[i/2][3], label=labels[i/2][3])
        
        cur_title = i%2 * 'Zoomed {}'.format(titles[i/2]) + (1 - i%2) * titles[i/2].capitalize()
        ax[ax_num, 0].set_title(cur_title, fontsize=14)
        
        ax[ax_num, 0].set_ylabel(y_labels[i/2], fontsize=10)
        
        bea[ax_num] = ax[ax_num, 0].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=14)
        
        if i%2:
            
            if limits[i/2] == None:
                
                raise Exception('No limits given for {}'.format(titles[i/2]))
                
            ax[ax_num, 0].set_ylim(limits[i/2])
        
        ax_num += 1
    
    ax[ax_num-1, 0].set_xlabel(time_unit, fontsize=10)
    
    #-----File naming-----------------------------------------------------------------------------------------------------
    
    file_name  = name_type + '_({0}, {1})'.format(int(Kw), int(Ka))
    
    file_name = if_file_exists(file_name, '.pdf')
    
    metadata = {'Author': '@tonyliveswell', 'Title': title_string, 'Subject': 'CubeSat dynamics and filtering',
                'Keywords': 'CubeSat, Kalman filter, Quaternion, Angular velocity, Orientation, Magnetorquers'}
    
    plt.savefig(file_name + '.pdf', format='pdf', bbox_extra_artists=bea, bbox_inches='tight', metadata=metadata)
    plt.show()
    
def save_filter_to_file(X, q0, Omega, N, T1, T5, Kw, Ka, J, alt, incl, m_MAX, P_0, sigma_M, B_bias, sigma_meas, storm_magn,
                       storm_interval, sigma_B_actual):
    
    roll, pitch, yaw = quat2rpy(q0, X[0], X[1], X[2])
    seconds = np.linspace(0, (N-1)*(T1+T5), N)
    
    if (sigma_M == 0) and (B_bias == 0) and (sigma_meas == 0) and ((storm_magn == 0) or (storm_magn is None)):
        
        title_string = 'Dynamics'
        file_string = 'dynamics'
        P_0_string = 'Initial covariance matrix P_0 = None\n'
        storm_interval_unit = ''
    else:
        title_string = 'Filter results'
        file_string = 'filter'
        P_0_string = 'Initial covariance matrix P_0 = diag[%5.4f, %5.4f, %5.4f, %5.4f, %5.4f, %5.4f] (rad2, rad2/s2)\n'\
                     % (P_0[0,0], P_0[1,1], P_0[2,2], P_0[3,3], P_0[4,4], P_0[5,5])
        storm_interval_unit = ' s'
    
    ARR = np.vstack((range(N), seconds, X[3:], Omega, roll, pitch, yaw, X[:3], q0)).T
    
    fmt = '%d\t%d\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f'
    title = u'{0}\n\nAltitude of the orbit h = {1} m\nInclination of the orbit i = {2} deg\n'\
             .format(title_string, int(alt), incl * 180. / np.pi) + \
             'Inertia tensor J = diag[{0}, {1}, {2}] kg*m2\nPD regulator coefficients (Kw, Ka) = ({3}, {4})\n'\
             .format(J[0,0], J[1,1], J[2,2], int(Kw), int(Ka)) + \
             'Control and measurement periods (T5, T1) = ({0}, {1}) s\nMaximal dipole moment u = {2} A*m2\n'\
             .format(T5, T1, m_MAX) + P_0_string + \
             'Standard deviation of internal torque noise sigma_M = {} N*m\n'\
             .format(sigma_M) + \
             'Bias of the magnetometer B_bias = {} T\n'\
             .format(B_bias) + \
             'Standard deviation of magnetometer measurements noise sigma_meas = {} T\n'\
             .format(sigma_meas) + \
             'Magnitude of the magnetic storm storm_magn = {0} T\nInterval of the magnetic storm = {1}{2}\n'\
             .format(storm_magn, storm_interval, storm_interval_unit) + \
             'Standard deviation of the noise, which simulates actual magnetic field sigma_B_actual = {} T\n\n'\
             .format(sigma_B_actual)
    rows = 'Iters\tSeconds\tAAV X\t\tAAV Y\t\tAAV Z\t\tRAV X\t\tRAV Y\t\tRAV Z\t\tRoll\t\tPitch\t\tYaw\t\t' + \
           'QUAT 1\t\tQUAT 2\t\tQUAT 3\t\tQUAT 0'
    header = title + rows
    
    file_name = '{0}_({1}, {2})'.format(file_string, int(Kw), int(Ka))
    
    file_name = if_file_exists(file_name, '.txt')
    
    np.savetxt(file_name + '.txt', ARR, fmt=fmt, header=header)
    
    print('File \'{}\' is created'.format(file_name + '.txt'))

def print_MSE(N, error_quat, error_omegas, error_rpy, M, X_0, T1, T5, J, Kw, Ka, alt, incl, m_MAX, P_0, sigma_M, B_bias,
              sigma_meas, storm_magn, sigma_B_actual, storm_interval, structure=[1,0,1,0,1,0,1,0], AAV_limits=None,
              RAV_limits=None, RPY_limits=None, QUAT_limits=None, add_q0=False, add_q0_abs=False, time_unit='hours',
              angle_unit='radians', title=True, units_in_title=True):
    
    num_of_plots = np.sum(structure)
    if not num_of_plots:
        raise Exception('No plots chosen')
        
    inds = list(compress(range(len(structure)), structure))
    
    bea = [0,] * num_of_plots
    
    time = np.linspace(0, (N-1)*(T1+T5), N)
    time_axis(time, time_unit)
        
    angle_mul = angle_axis(angle_unit)
    
    #-------------Aggregation-----------------------------------------------------------------------------------------------
    
    ERR_cmp = [error_omegas[:3] * angle_mul, error_omegas[3:] * angle_mul, error_rpy * angle_mul, error_quat]
    
    labels_AAV = ['$\omega_1$', '$\omega_2$', '$\omega_3$']
    labels_RAV = ['$\Omega_1$', '$\Omega_2$', '$\Omega_3$']
    labels_RPY = ['Roll', 'Pitch', 'Yaw']
    labels_QUAT = ['$q_1$', '$q_2$', '$q_3$', '$q_0$', '$\\left| q_0 \\right|$']
    
    labels = [labels_AAV, labels_RAV, labels_RPY, labels_QUAT]
    
    titles = ['absolute angular velocity', 'relative angular velocity', 'orientation', 'quaternion components']
    
    y_labels = ['{}/s'.format(angle_unit[:3]), '{}/s'.format(angle_unit[:3]), angle_unit, 'units']
    
    limits = [AAV_limits, RAV_limits, RPY_limits, QUAT_limits]
    
    #-----Adjusting subplots----------------------------------------------------------------------------------------------
    
    adj_arr = [0.5, 0.643, 0.721, 0.77, 0.803, 0.827, 0.8455, 0.86]
    subpl_shift = 4
        
    #------Creating subplots-----------------------------------------------------------------------------------------------
    
    f, ax = plt.subplots(num_of_plots, 1, sharex=True, figsize=(12, num_of_plots * 3 + subpl_shift), dpi=300, squeeze=False)
    
    f.subplots_adjust(top = adj_arr[num_of_plots-1])
    
    if (not title) and (units_in_title): warn('Can\'t put units in non-existing title')
    
    if title:
        
        #-----------Units in title-------------------------------------------------------------------------------------------
        
        if units_in_title:
            radps = '\\left[ {\\large \\textsf{rad}} / {\\large \\textsf{s}} \\right]'
            Am2 = '\\left[ {\\large \\textsf{A}}\!\cdot\! \
                   {\\large \\textsf{m}}^{\\small \\textsf{2}} \\right]'
            kgm2 = '\\left[ {\\large \\textsf{kg}}\!\cdot\! \
                    {\\large \\textsf{m}}^{\\small \\textsf{2}} \\right]'
            NmpT2 = '\\left[ {\\large \\textsf{N}}\!\cdot\!{\\large \\textsf{m}} / {\\large \\textsf{T}}^\
                    {\\small \\textsf{ 2}} \\right]'
            km = '\\left[ {\\large \\textsf{km}} \\right]'
            sec = '\\left[ {\\large \\textsf{s}} \\right]'
            P_unit = '\\left[ {\\large \\textsf{rad}}^{\\small \\textsf{2}} (\!\\times 3), {\\large \\textsf{rad}}^{\\small \
                      \\textsf{2}} / {\\large \\textsf{s}}^{\\small \\textsf{2}} (\!\\times 3) \\right]'
            hour = '\\left[ {\\large \\textsf{h}} \\right]'
            Nm = '\\left[ {\\large \\textsf{N}}\!\cdot\!{\\large \\textsf{m}} \\right]'
            tesla = '\\left[ {\\large \\textsf{T}} \\right]'
            minute = '\\left[ {\\large \\textsf{min}} \\right]'
            day = '\\left[ {\\large \\textsf{days}} \\right]'
        else:
            radps = Am2 = kgm2 = NmpT2 = km = sec = P_unit = hour = Nm = tesla = minute = day = ''
        st_int_unit = [sec, minute, hour, day]
            
        #-----Initial covariance matrix in title-----------------------------------------------------------------------
            
        P_init_string = '$$P^{{\\normalsize \\textsf{ init}}} = {\\Large \\textsf{diag}} (%5.2f, %5.2f, %5.2f, \
                        %5.2f, %5.2f, %5.2f) %s$$' % (P_0[0,0], P_0[1,1], P_0[2,2], P_0[3,3], P_0[4,4], P_0[5,5], P_unit)
        
        #-----Internal torque noise in title---------------------------------------------------------------------------

        if sigma_M == 0:
            torq_noise_string = '{\\large \\textsf{No internal torque noise in the model}}'
        else:
            torq_noise_string = '\\eta_{{\\normalsize \\textsf{ torque}}} \\sim \\mathcal{N} \\left(0, \{%1.0e\}^2 \
                                     \\right) %s' % (sigma_M, Nm)

        #------Magnetometer noise in title----------------------------------------------------------------------------

        if (B_bias != 0) or (sigma_meas != 0):
            if sigma_meas == 0:
                meas_noise_string = '\\eta_{{\\normalsize \\textsf{ magnetometer}}} \\sim \\delta \\left( %1.0e \\right) \
                                         %s' % (B_bias, tesla)
            else:
                if B_bias != 0:
                    B_bias_string = '%1.0e' % B_bias
                else:
                    B_bias_string = '0'
                meas_noise_string = '\\eta_{{\\normalsize \\textsf{ magnetometer}}} \\sim \\mathcal{N} \\left(' +\
                                    B_bias_string + ', \{%1.0e\}^2 \\right) %s' % (sigma_meas, tesla)
        else:
            meas_noise_string = '{\\large \\textsf{No measurements\' noise in the model}}'

        #-----Magnetic storm in title---------------------------------------------------------------------------------

        if isinstance(storm_interval, list) or isinstance(storm_interval, tuple):
            if len(storm_interval) == 2:
                st_int_string = time_label(storm_interval[0], storm_interval[1], st_int_unit, time_unit)

                storm_string = '$$\\eta_{{\\normalsize \\textsf{ storm}}} \\sim \\mathcal{N} \\left(%1.0e, \{%1.0e\}^2 \
                \\right) %s \qquad \\Delta T_{{\\normalsize \\textsf{ storm}}} = '\
                % (storm_magn, sigma_B_actual, tesla) + st_int_string
            else:
                raise Exception('Interval of magnetic storm is in invalid form (not 2 entries)')
        else:
            if (storm_magn is None) or (storm_magn == 0):
                storm_string = '{\\large \\textsf{No magnetic storm in the model}}'
            else:
                raise Exception('No interval is given for magnetic storm')
            
        #-----Overall noise in title----------------------------------------------------------------------------------
            
        noise_string = '\n' + P_init_string + '\n$$' + torq_noise_string + '\qquad' + meas_noise_string + '$$\n' + \
                        storm_string
        
        q0 = np.sqrt(1. - X_0[0]**2 - X_0[1]**2 - X_0[2]**2)
        
        sup=f.suptitle('{\\huge \\textsc{True estimation errors in %d cycles}}\n$$q^{{\\normalsize \\textsf{init}}} = \
                       (%1.1f, %1.1f, %1.1f, %1.1f) \qquad \omega^{{\\normalsize \\textsf{init}}} = (%5.1f, %5.1f, %5.1f) %s\
                       $$\n$$J = {\\Large \\textsf{diag}}(%5.3f, %5.3f, %5.3f) %s \qquad (k\'_\omega, k_a) = (%5.1i, %5.1i) %s\
                       \qquad \\mu_{\\max} = %5.1f %s$$\n$$h_{{\\normalsize \\textsf{orb}}} = %5.1i %s \qquad i = \
                       %5.1f^{\\circ} \qquad (T_1, T_5) = (%5.1f, %5.1f) %s$$'
                       % (M, q0, X_0[0], X_0[1], X_0[2], X_0[3], X_0[4], X_0[5], radps, J[0,0], J[1,1], J[2,2], \
                          kgm2, int(Kw), int(Ka), NmpT2, m_MAX, Am2, alt / 1000, km, incl * 180 / np.pi, T1, T5, sec) + \
                          noise_string, 
                          fontsize=18, linespacing = 1.7)
    
        bea += [sup,]
        
    ax_num = 0
        
    for i in inds:
        
        for j in range(3):
            
            ax[ax_num, 0].plot(time, ERR_cmp[i/2][j], label=labels[i/2][j])
            
        if (i/2 == 3) and add_q0:
            
            ax[ax_num, 0].plot(time, ERR_cmp[i/2][3], label=labels[i/2][3])
        
        if (i/2 == 3) and add_q0_abs:
            
            ax[ax_num, 0].plot(time, ERR_cmp[i/2][4], label=labels[i/2][4])
        
        cur_title = i%2 * 'Zoomed {}'.format(titles[i/2]) + (1 - i%2) * titles[i/2].capitalize() + ' MSE'
        ax[ax_num, 0].set_title(cur_title, fontsize=14)
        
        ax[ax_num, 0].set_ylabel(y_labels[i/2], fontsize=10)
        
        bea[ax_num] = ax[ax_num, 0].legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0., fontsize=14)
        
        if i%2:
            
            if limits[i/2] == None:
                
                raise Exception('No limits given for {}'.format(titles[i/2]))
                
            ax[ax_num, 0].set_ylim(limits[i/2])
        
        ax_num += 1
    
    ax[ax_num-1, 0].set_xlabel(time_unit, fontsize=10)
    
    #-----File naming-----------------------------------------------------------------------------------------------------
    
    file_name  = 'mse_({0}, {1})'.format(int(Kw), int(Ka))
    
    file_name = if_file_exists(file_name, '.pdf')
    
    metadata = {'Author': '@tonyliveswell', 'Title': 'True estimation error', 'Subject': 'CubeSat dynamics and filtering',
                'Keywords': 'CubeSat, Kalman filter, Quaternion, Angular velocity, Orientation, Magnetorquers'}
    
    plt.savefig(file_name + '.pdf', format='pdf', bbox_extra_artists=bea, bbox_inches='tight', metadata=metadata)
    plt.show()
    
def save_MSE_to_file(N, error_quat, error_omegas, error_rpy, M, X_0, T1, T5, Kw, Ka, J, alt, incl, m_MAX, P_0, sigma_M, B_bias,
                     sigma_meas, storm_magn, storm_interval, sigma_B_actual):
    
    seconds = np.linspace(0, (N-1)*(T1+T5), N)
    
    title_string = 'True estimation errors in {} cycles'.format(M)
    P_0_string = 'Initial covariance matrix P_0 = diag[%5.4f, %5.4f, %5.4f, %5.4f, %5.4f, %5.4f] (rad2, rad2/s2)\n'\
                 % (P_0[0,0], P_0[1,1], P_0[2,2], P_0[3,3], P_0[4,4], P_0[5,5])
    if storm_interval is None:
        storm_interval_unit = ''
    else:
        storm_interval_unit = ' s'
    
    ARR = np.vstack((range(N), seconds, error_omegas, error_rpy, error_quat)).T
    
    fmt = '%d\t%d\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f\t' + \
          '%5.10f\t%5.10f\t%5.10f\t%5.10f\t%5.10f'
    title = u'{0}\n\nAltitude of the orbit h = {1} m\nInclination of the orbit i = {2} deg\n'\
             .format(title_string, int(alt), incl * 180. / np.pi) + \
             'Inertia tensor J = diag[{0}, {1}, {2}] kg*m2\nPD regulator coefficients (Kw, Ka) = ({3}, {4})\n'\
             .format(J[0,0], J[1,1], J[2,2], int(Kw), int(Ka)) + \
             'Control and measurement periods (T5, T1) = ({0}, {1}) s\nMaximal dipole moment u = {2} A*m2\n'\
             .format(T5, T1, m_MAX) + P_0_string + \
             'Standard deviation of internal torque noise sigma_M = {} N*m\n'\
             .format(sigma_M) + \
             'Bias of the magnetometer B_bias = {} T\n'\
             .format(B_bias) + \
             'Standard deviation of magnetometer measurements noise sigma_meas = {} T\n'\
             .format(sigma_meas) + \
             'Magnitude of the magnetic storm storm_magn = {0} T\nInterval of the magnetic storm = {1}{2}\n'\
             .format(storm_magn, storm_interval, storm_interval_unit) + \
             'Standard deviation of the noise, which simulates actual magnetic field sigma_B_actual = {} T\n\n'\
             .format(sigma_B_actual)
    rows = 'Iters\tSeconds\tAAV X\t\tAAV Y\t\tAAV Z\t\tRAV X\t\tRAV Y\t\tRAV Z\t\tRoll\t\tPitch\t\tYaw\t\t' + \
           'QUAT 1\t\tQUAT 2\t\tQUAT 3\t\tQUAT 0\t\t|QUAT 0|'
    header = title + rows
    
    file_name = 'mse_({0}, {1})'.format(int(Kw), int(Ka))
    
    file_name = if_file_exists(file_name, '.txt')
    
    np.savetxt(file_name + '.txt', ARR, fmt=fmt, header=header)
    
    print('File \'{}\' is created'.format(file_name + '.txt'))