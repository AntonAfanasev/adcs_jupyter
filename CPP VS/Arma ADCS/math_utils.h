#pragma once

#include <armadillo>

arma::Mat<double> skew_symm(arma::Col<double> vec);
arma::Mat<double> symm(arma::Col<double> vec);
arma::Col<double> quat_product(arma::Col<double> q1, arma::Col<double> q2);
arma::Col<double> rot_q(arma::Col<double> q, arma::Col<double> vec);
arma::Col<double> omega_to_Omega(arma::Col<double> omega, arma::Col<double> quat, double mean_motion);
arma::Col<double> produce_moment(double Kw, double Ks, double mean_motion, double m_max, arma::Col<double> S, arma::Col<double> Omega, arma::Col<double> B_body_model, arma::Col<double> m_res_est);
arma::Col<double> cumulative_torque(arma::Col<double> quat, arma::Mat<double> J, double mean_motion, double sigma_torque, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl);
arma::Col<double> rk4_step(arma::Col<double> X_n, double h, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl, std::function<arma::Col<double>(arma::Col<double>, double, double, arma::Mat<double>, arma::Mat<double>, arma::Col<double>, arma::Col<double>, bool)> func);
arma::Col<double> rk4(arma::Col<double> X_0, double t, const int n, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl, std::function<arma::Col<double>(arma::Col<double>, double, double, arma::Mat<double>, arma::Mat<double>, arma::Col<double>, arma::Col<double>, bool)> func);
arma::Col<double> motion_equation(arma::Col<double> x, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl);
arma::Col<double> solve_motion_equation(arma::Col<double> state, double t, const int n, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl);
