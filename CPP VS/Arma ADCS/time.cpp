#include "time.h"

std::map<int, int> MONTHS_PASSED_LEAP = { { 0, 0 },
										{ 1, 31 },
										{ 2, 60 },
										{ 3, 91 },
										{ 4, 121 },
										{ 5, 152 },
										{ 6, 182 },
										{ 7, 213 },
										{ 8, 244 },
										{ 9, 274 },
										{ 10, 305 },
										{ 11, 335 } };

std::map<int, int> MONTHS_PASSED = { { 0, 0 },
									{ 1, 31 },
									{ 2, 59 },
									{ 3, 90 },
									{ 4, 120 },
									{ 5, 151 },
									{ 6, 181 },
									{ 7, 212 },
									{ 8, 243 },
									{ 9, 273 },
									{ 10, 304 },
									{ 11, 334 } };

double JD_add_seconds(double seconds)
{
	return seconds / 24 / 3600;
}
double julian_centuries(double JD)
{
	return (JD - 2451545) / 36525;
}

double gregorian_to_julian(arma::Col<double> GD)
{
	double year = GD(0);
	double month = GD(1);
	double day = GD(2);
	double hour = GD(3);
	double min = GD(4);
	double sec = GD(5);

	if (month <= 2)
	{
		year -= 1;
		month += 12;
	}

	return int(365.25 * year) + int(30.6001 * (month + 1)) + day + 1720981.5 + hour / 24 + min / 24 / 60 + sec / 24 / 3600;
}

arma::Col<double> julian_to_gregorian(double JD)
{
	int b = int(JD + 0.5) + 1537;
	int c = int((b - 122.1) / 365.25);
	int d = int(365.25 * c);
	int e = int((b - d) / 30.6001);

	double hour = (JD + 0.5 - int(JD + 0.5)) * 24;
	double min = (hour - int(hour)) * 60;
	hour = int(hour);
	double sec = (min - int(min)) * 60;
	min = int(min);
	double day = b - d - int(30.6001 * e);
	double month = e - 1 - 12 * int(e / 14);
	double year = c - 4715 - int((month + 7) / 10);

	arma::Col<double> GD = { year, month, day, hour, min, sec };

	return GD;
}

double julian_to_year_frac(double JD)
{
	arma::Col<double> GD = julian_to_gregorian(JD);
	int year = int(GD(0));
	int month = int(GD(1));
	double day = GD(2);
	double hour = GD(3);
	double min = GD(4);
	double sec = GD(5);

	if (year % 4 == 0)
	{
		return year + (MONTHS_PASSED_LEAP[month - 1] + day) / 366 + (hour + min / 60 + sec / 3600) / 366 / 24;
	}
	else
	{
		return year + (MONTHS_PASSED[month - 1] + day) / 365 + (hour + min / 60 + sec / 3600) / 365 / 24;
	}
}

double mjd(double JD)
{
	return JD - 2400000.5;
}

double jul_date_ut1(double JC, double DUT1)
{
	return 36525 * JC + DUT1 / 24 / 3600;
}