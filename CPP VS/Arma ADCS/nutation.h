#pragma once

#include <armadillo>

const double DEG2RAD = arma::datum::pi / 180;
const double ARCSEC2RAD = DEG2RAD / 3600;
const double UARCSEC2RAD = ARCSEC2RAD / 1000000;

arma::Mat<double> DCM_nut_prec(double JC);
