#pragma once

#include <armadillo>

class SunSensors
{
public:
	double get_sigma_sun();
	void set_sigma_sun(double new_sigma_sun);
	double get_ss_angle();
	void set_ss_angle(double new_ss_angle);
	arma::Mat<double> get_normals();
	void set_normals(arma::Mat<double> new_normals);

	arma::Col<double> ss_measure(arma::Col<double> S_body_true);
	bool inSight(arma::Col<double> S_body_model);

private:
	double sigma_sun = 0;
	double ss_angle = 0;
	arma::Mat<double> normals = arma::Mat<double>{ {1, 0, 0},
												   {0, 1, 0},
												   {0, 0, 1},
												   {-1, 0, 0},
												   {0, -1, 0} };
};

