#pragma once

#include <armadillo>
#include <map>

double JD_add_seconds(double seconds);
double julian_centuries(double JD);
double gregorian_to_julian(arma::Col<double> GD);
arma::Col<double> julian_to_gregorian(double JD);
double julian_to_year_frac(double JD);
double mjd(double JD);
double jul_date_ut1(double JC, double DUT1);