#pragma once

#include <armadillo>

const double MU = 3.986004418e+14;
const double R_e = 6371e+3;
const double J2 = 0.00108263;

arma::Col<double> node_vector_from_ang_momentum(arma::Col<double> h);
arma::Col<double> ecc_from_rv(arma::Col<double> R_ECI, arma::Col<double> V_ECI, double R);
double spec_energy_from_rv(arma::Col<double> V_ECI, double R);
double sma_from_spec_energy(double xi);
double semi_from_sma_and_ecc(double a, double e);
double mean_motion_from_sma(double a);
double incl_from_ang_momentum(arma::Col<double> h);
double RAAN_true_aop_true_long(arma::Col<double> vec);
double arg_lat_OR_aop(arma::Col<double> vec, double vec_abs, arma::Col<double> n, double n_abs);
double true_anom_from_ecc(arma::Col<double> e_vec, double e, arma::Col<double> R_ECI, double R, arma::Col<double> V_ECI);
arma::Col<double> RV2COE(arma::Col<double> r_ECI, arma::Col<double> v_ECI);

double eccentric_anom_from_true_anom(double nu, double e);
double mean_anom_from_eccentric_anom(double E, double e);
double update_anomalies(double ecc, double true_anom, double arg_lat, double true_long);

double sma_perturbation(double a, double n_dot_2, double T, double omega_0);
double ecc_perturbation(double e, double n_dot_2, double T, double omega_0);
double RAAN_perturbation(double T, double omega_0, double i, double p);
double aop_perturbation(double T, double omega_0, double i, double p);
double mean_anom_increment(double n_dot_2, double n_dot_dot_6, double T, double omega_0);
arma::Col<double> update_for_perturbations(double T, arma::Col<double> COE_vec, double mean_anom, double n_dot_2, double n_dot_dot_6);

double eccentric_anom_from_mean_anom(double M, double e);
double true_anom_from_eccentric_anom(double E, double e);
arma::Col<double> update_anomalies_back(double ecc, double mean_anom, double true_anom, double arg_lat, double true_long);

arma::Mat<double> DCM_perifocal_to_ECI(double RAAN, double i, double aop);
arma::Col<double> R_perifocal(double nu, double e, double p);
arma::Col<double> V_perifocal(double nu, double e, double p);
arma::Col<double> COE2RV(double sma, double ecc, double incl, double RAAN, double arg_lat, double aop, double true_aop, double true_anom, double true_long, double semiparameter);

arma::Col<double> move(arma::Col<double> R_ECI, arma::Col<double> V_ECI, double T, double n_dot_2, double n_dot_dot_6);