#pragma once

#include <armadillo>

class Gyroscope
{
public:
	double get_sigma_gyr();
	void set_sigma_gyr(double new_sigma_gyr);
	arma::Col<double> get_gyr_bias();
	void set_gyr_bias(arma::Col<double> new_gyr_bias);

	arma::Col<double> gyr_measure(arma::Col<double> omega);

private:
	double sigma_gyr = 0;
	arma::Col<double> gyr_bias = arma::Col<double>(3, arma::fill::zeros);
};

