#include "Magnetometer.h"

double Magnetometer::get_sigma_magn()
{
	return sigma_magn;
}

void  Magnetometer::set_sigma_magn(double new_sigma_magn)
{
	sigma_magn = new_sigma_magn;
}

arma::Col<double> Magnetometer::get_mm_bias()
{
	return mm_bias;
}

void Magnetometer::set_mm_bias(arma::Col<double> new_mm_bias)
{
	mm_bias = new_mm_bias;
}

arma::Col<double> Magnetometer::mm_measure(arma::Col<double> B_body_true)
{
	return B_body_true + mm_bias + arma::randn(3) * sigma_magn;
}