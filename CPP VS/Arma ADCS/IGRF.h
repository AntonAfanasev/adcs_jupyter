#pragma once

#include <armadillo>

arma::Col<double> magn_field_ECI_from_spher(double JD_UTC, double r, double theta, double phi);

class MagneticField
{
public:
	double get_sigma_magnetic_field();
	void set_sigma_magnetic_field(double new_sigma_mf);
	arma::Col<double> magn_field_ECI(double JD_UTC, arma::Col<double> R_ECI);
	arma::Col<double> magn_field_true(arma::Col<double> B_model);

private:
	double sigma_mf = 0;
};
