#pragma once

#include <armadillo>

class Magnetometer
{
public:
	double get_sigma_magn();
	void set_sigma_magn(double new_sigma_magn);
	arma::Col<double> get_mm_bias();
	void set_mm_bias(arma::Col<double> new_mm_bias);

	arma::Col<double> mm_measure(arma::Col<double> B_body_true);

private:
	double sigma_magn = 0;
	arma::Col<double> mm_bias = arma::Col<double>(3, arma::fill::zeros);
};

