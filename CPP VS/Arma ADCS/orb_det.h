#pragma once

#include "sofa.h"
#include "sofam.h"
# include "SGP4.h"

void DCM2QUAT(double DCM[3][3], double quat[4]);
void QUAT2DCM(double quat[4], double DCM[3][3]);

void J20002ITRS_DCM(double JD_UTC, double DCM_J2000_to_ITRS[3][3]);
void J20002ITRS_RV_from_JD(double JD_UTC, double r_J2000[3], double v_J2000[3], double r_ITRS[3], double v_ITRS[3]);
void J20002ITRS_RV_from_DCM(double DCM_J2000_to_ITRS[3][3], double r_J2000[3], double v_J2000[3], double r_ITRS[3], double v_ITRS[3]);
void ITRS2J2000_DCM(double JD_UTC, double DCM_ITRS_to_J2000[3][3]);
void ITRS2J2000_RV_from_JD(double JD_UTC, double r_ITRS[3], double v_ITRS[3], double r_J2000[3], double v_J2000[3]);
void ITRS2J2000_RV_from_DCM(double DCM_ITRS_to_J2000[3][3], double r_ITRS[3], double v_ITRS[3], double r_J2000[3], double v_J2000[3]);

void WGS842ITRS_R(double elong, double phi, double height, double r_ITRS[3]);
void ITRS2WGS84_R(double r_ITRS[3], double* elong, double* phi, double* height);
void WGS842ITRS_V(double v_WGS84[3], double elong, double phi, double height, double v_ITRS[3]); // v_WGS84 = { East, North, Up }
void ITRS2WGS84_V(double v_ITRS[3], double elong, double phi, double height, double v_WGS84[3]);

void J20002LVLH_DCM(double r_J2000[3], double v_J2000[3], double DCM_J2000_to_LVLH[3][3]);
void J20002LVLH_Q(double r_J2000[3], double v_J2000[3], double Q_J2000_to_LVLH[4]);
void J20002LVLH_RV(double r_J2000[3], double v_J2000[3], double r_LVLH[3], double v_LVLH[3]);
void J20002LVLH_RV_from_DCM(double DCM_J2000_to_LVLH[3][3], double r_J2000[3], double v_J2000[3], double r_LVLH[3], double v_LVLH[3]);

void TEME2ITRS_DCM(double JD_UTC, double DCM_TEME_to_ITRS[3][3]);
void TEME2ITRS_RV_from_JD(double JD_UTC, double r_TEME[3], double v_TEME[3], double r_ITRS[3], double v_ITRS[3]);
void TEME2ITRS_RV_from_DCM(double DCM_TEME_to_ITRS[3][3], double r_TEME[3], double v_TEME[3], double r_ITRS[3], double v_ITRS[3]);
void ITRS2TEME_DCM(double JD_UTC, double DCM_ITRS_to_TEME[3][3]);
void ITRS2TEME_RV_from_JD(double JD_UTC, double r_ITRS[3], double v_ITRS[3], double r_TEME[3], double v_TEME[3]);
void ITRS2TEME_RV_from_DCM(double DCM_ITRS_to_TEME[3][3], double r_ITRS[3], double v_ITRS[3], double r_TEME[3], double v_TEME[3]);

void rv2el(double* rr, double* vv, double tsince, char tle1[], char tle2[]); // tsince in mins