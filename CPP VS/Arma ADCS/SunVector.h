#pragma once

#include <armadillo>
#include "time.h"

class SunVector
{
public:
	double get_sigma_sun_vector();
	void set_sigma_sun_vector(double new_sigma_sun_vector);
	arma::Col<double> sun_vector_model(double JD_UTC, double DUT1);
	arma::Col<double> sun_vector_true(arma::Col<double> S_model);

private:
	double sigma_sun_vector;
};

