#include "propagation.h"

arma::Col<double> node_vector_from_ang_momentum(arma::Col<double> h)
{
	return arma::Col<double>{ -h(1), h(0), 0 };
}

arma::Col<double> ecc_from_rv(arma::Col<double> R_ECI, arma::Col<double> V_ECI, double R)
{
	return ((arma::dot(V_ECI, V_ECI) - MU / R) * R_ECI - arma::dot(R_ECI, V_ECI) * V_ECI) / MU;
}

double spec_energy_from_rv(arma::Col<double> V_ECI, double R)
{
	return arma::dot(V_ECI, V_ECI) / 2 - MU / R;
}

double sma_from_spec_energy(double xi)
{
	return -MU / 2 / xi;
}

double semi_from_sma_and_ecc(double a, double e)
{
	return a * (1 - pow(e, 2));
}

double mean_motion_from_sma(double a)
{
	return sqrt(MU / pow(a, 3));
}

double incl_from_ang_momentum(arma::Col<double> h)
{
	arma::Col<double> h_norm = arma::normalise(h);
	double cos_incl = h_norm(2);

	double incl = 0;

	if (abs(cos_incl) >= 1)
	{
		if (cos_incl < 0)
		{
			incl = arma::datum::pi;
		}
	}
	else
	{
		incl = acos(cos_incl);
	}

	return incl;
}

double RAAN_true_aop_true_long(arma::Col<double> vec)
{
	arma::Col<double> vec_norm = arma::normalise(vec);
	double cos_param = vec_norm(0);

	double param = 0;

	if (abs(cos_param) >= 1)
	{
		if (cos_param < 0)
		{
			param = arma::datum::pi;
		}
	}
	else
	{
		param = acos(cos_param);
	}

	if (vec(1) < 0)
	{
		return 2 * arma::datum::pi - param;
	}
	else
	{
		return param;
	}
}

double arg_lat_OR_aop(arma::Col<double> vec, double vec_abs, arma::Col<double> n, double n_abs)
{
	double cos_param = arma::dot(n, vec) / vec_abs / n_abs;

	double param = 0;

	if (abs(cos_param) >= 1)
	{
		if (cos_param < 0)
		{
			param = arma::datum::pi;
		}
	}
	else
	{
		param = acos(cos_param);
	}

	if (vec(2) < 0)
	{
		return 2 * arma::datum::pi - param;
	}
	else
	{
		return param;
	}
}

double true_anom_from_ecc(arma::Col<double> e_vec, double e, arma::Col<double> R_ECI, double R, arma::Col<double> V_ECI)
{
	double cos_nu = arma::dot(e_vec, R_ECI) / R / e;

	double nu = 0;

	if (abs(cos_nu) >= 1)
	{
		if (cos_nu < 0)
		{
			nu = arma::datum::pi;
		}
	}
	else
	{
		nu = acos(cos_nu);
	}

	if (arma::dot(R_ECI, V_ECI) < 0)
	{
		return 2 * arma::datum::pi - nu;
	}
	else
	{
		return nu;
	}
}

arma::Col<double> RV2COE(arma::Col<double> r_ECI, arma::Col<double> v_ECI)
{
	arma::Col<double> ang_momentum = arma::cross(r_ECI, v_ECI);
	arma::Col<double> node_vector = node_vector_from_ang_momentum(ang_momentum);
	double node_vector_norm = arma::norm(node_vector);
	double r_ECI_norm = arma::norm(r_ECI);
	arma::Col<double> ecc_vector = ecc_from_rv(r_ECI, v_ECI, r_ECI_norm);
	double ecc = arma::norm(ecc_vector);
	double spec_energy = spec_energy_from_rv(v_ECI, r_ECI_norm);

	if (ecc >= 1)
	{
		throw "Non-elliptical trajectory";
	}

	double sma = sma_from_spec_energy(spec_energy);
	double semiparameter = semi_from_sma_and_ecc(sma, ecc);
	double mean_motion = mean_motion_from_sma(sma);
	double incl = incl_from_ang_momentum(ang_momentum);

	double RAAN = arma::datum::nan;
	double arg_lat = arma::datum::nan;
	double aop = arma::datum::nan;
	double true_aop = arma::datum::nan;
	double true_anom = arma::datum::nan;

	if (abs(incl) >= 1e-15)
	{
		RAAN = RAAN_true_aop_true_long(node_vector);
		arg_lat = arg_lat_OR_aop(r_ECI, r_ECI_norm, node_vector, node_vector_norm);

		if (ecc >= 1e-15)
		{
			aop = arg_lat_OR_aop(ecc_vector, ecc, node_vector, node_vector_norm);
			true_aop = RAAN_true_aop_true_long(ecc_vector);
			true_anom = true_anom_from_ecc(ecc_vector, ecc, r_ECI, r_ECI_norm, v_ECI);
		}
	}
	else
	{
		if (ecc >= 1e-15)
		{
			true_aop = RAAN_true_aop_true_long(ecc_vector);
			true_anom = true_anom_from_ecc(ecc_vector, ecc, r_ECI, r_ECI_norm, v_ECI);
		}
	}

	double true_long = RAAN_true_aop_true_long(r_ECI);

	return arma::Col<double> { sma, ecc, incl, RAAN, arg_lat, aop, true_aop, true_anom, true_long, mean_motion, semiparameter };
}

double eccentric_anom_from_true_anom(double nu, double e)
{
	double cos_E = (e + cos(nu)) / (1 + e * cos(nu));

	double E = 0;

	if (abs(cos_E) >= 1)
	{
		if (cos_E < 0)
		{
			E = arma::datum::pi;
		}
	}
	else
	{
		E = acos(cos_E);
	}

	if (sin(nu) < 0)
	{
		return 2 * arma::datum::pi - E;
	}
	else
	{
		return E;
	}
}

double mean_anom_from_eccentric_anom(double E, double e)
{
	return E - e * sin(E);
}

double update_anomalies(double ecc, double true_anom, double arg_lat, double true_long)
{
	double eccentric_anom = 0;

	if (ecc >= 1e-15)
	{
		eccentric_anom = eccentric_anom_from_true_anom(true_anom, ecc);
	}
	else if (std::isfinite(arg_lat))
	{
		eccentric_anom = arg_lat;
	}
	else
	{
		eccentric_anom = true_long;
	}

	return mean_anom_from_eccentric_anom(eccentric_anom, ecc);
}

double sma_perturbation(double a, double n_dot_2, double T, double omega_0)
{
	return -4 * a * n_dot_2 * T / 3 / omega_0;
}

double ecc_perturbation(double e, double n_dot_2, double T, double omega_0)
{
	return -4 * (1 - e) * n_dot_2 * T / 3 / omega_0;
}

double RAAN_perturbation(double T, double omega_0, double i, double p)
{
	return -3 * omega_0 * pow(R_e, 2) * J2 * cos(i) * T / 2 / pow(p, 2);
}

double aop_perturbation(double T, double omega_0, double i, double p)
{
	return 3 * omega_0 * pow(R_e, 2) * J2 * (4 - 5 * pow(sin(i), 2)) * T / 4 / pow(p, 2);
}

double mean_anom_increment(double n_dot_2, double n_dot_dot_6, double T, double omega_0)
{
	return omega_0 * T + n_dot_2 * pow(T, 2) + n_dot_dot_6 * pow(T, 3);
}

arma::Col<double> update_for_perturbations(double T, arma::Col<double> COE_vec, double mean_anom, double n_dot_2, double n_dot_dot_6)
{
	double sma = COE_vec(0);
	double ecc = COE_vec(1);
	double incl = COE_vec(2);
	double RAAN = COE_vec(3);
	double arg_lat = COE_vec(4);
	double aop = COE_vec(5);
	double true_aop = COE_vec(6);
	double true_anom = COE_vec(7);
	double true_long = COE_vec(8);
	double mean_motion = COE_vec(9);
	double semiparameter = COE_vec(10);

	double sma_pert = sma_perturbation(sma, n_dot_2, T, mean_motion);
	double ecc_pert = ecc_perturbation(ecc, n_dot_2, T, mean_motion);
	double RAAN_pert = RAAN_perturbation(T, mean_motion, incl, semiparameter);
	double aop_pert = aop_perturbation(T, mean_motion, incl, semiparameter);
	double mean_anom_diff = mean_anom_increment(n_dot_2, n_dot_dot_6, T, mean_motion);

	sma += sma_pert;
	ecc += ecc_pert;

	if ((!std::isfinite(RAAN)) && (!std::isfinite(true_aop)))
	{
		true_long += RAAN_pert + aop_pert;
	}
	else if ((!std::isfinite(RAAN)) && (std::isfinite(true_aop)))
	{
		true_aop += RAAN_pert + aop_pert;
	}
	else if ((std::isfinite(RAAN)) && (!std::isfinite(true_aop)))
	{
		arg_lat += aop_pert;
		RAAN += RAAN_pert;
	}
	else
	{
		aop += aop_pert;
		RAAN += RAAN_pert;
	}

	semiparameter = semi_from_sma_and_ecc(sma, ecc);

	return arma::Col<double> { sma, ecc, incl, RAAN, arg_lat, aop, true_aop, true_anom, true_long, mean_motion, semiparameter, mean_anom + mean_anom_diff };
}

double eccentric_anom_from_mean_anom(double M, double e)
{
	double E = 0;

	if (((M < 0) && (M > -arma::datum::pi)) || (M > arma::datum::pi))
	{
		E = M - e;
	}
	else
	{
		E = M + e;
	}

	double E_prev = E + 1;

	while (abs(E - E_prev) > 1e-10)
	{
		E_prev = E;
		E = E_prev + (M - E_prev + e * sin(E_prev)) / (1 - e * cos(E_prev));
	}

	return E;
}

double true_anom_from_eccentric_anom(double E, double e)
{
	double cos_nu = (cos(E) - e) / (1 - e * cos(E));

	double nu = 0;

	if (abs(cos_nu) >= 1)
	{
		if (cos_nu < 0)
		{
			nu = arma::datum::pi;
		}
	}
	else
	{
		nu = acos(cos_nu);
	}

	if (sin(E) < 0)
	{
		return 2 * arma::datum::pi - nu;
	}
	else
	{
		return nu;
	}
}

arma::Col<double> update_anomalies_back(double ecc, double mean_anom, double true_anom, double arg_lat, double true_long)
{
	double eccentric_anom = eccentric_anom_from_mean_anom(mean_anom, ecc);

	if (ecc >= 1e-15)
	{
		return arma::Col<double> { true_anom_from_eccentric_anom(eccentric_anom, ecc), arg_lat, true_long };
	}
	else if (std::isfinite(arg_lat))
	{
		return arma::Col<double> { true_anom, eccentric_anom, true_long };
	}
	else
	{
		return arma::Col<double> { true_anom, arg_lat, eccentric_anom };
	}
}

arma::Mat<double> DCM_perifocal_to_ECI(double RAAN, double i, double aop)
{
	return arma::Mat<double> { { cos(RAAN) * cos(aop) - sin(RAAN) * sin(aop) * cos(i),
								 -cos(RAAN) * sin(aop) - sin(RAAN) * cos(aop) * cos(i),
								 sin(RAAN) * sin(i) },
							   { sin(RAAN) * cos(aop) + cos(RAAN) * sin(aop) * cos(i),
								 -sin(RAAN) * sin(aop) + cos(RAAN) * cos(aop) * cos(i),
								 -cos(RAAN) * sin(i) },
							   { sin(aop) * sin(i), cos(aop) * sin(i), cos(i) } };
}

arma::Col<double> R_perifocal(double nu, double e, double p)
{
	double mul = p / (1 + e * cos(nu));

	return mul * arma::Col<double> { cos(nu), sin(nu), 0 };
}

arma::Col<double> V_perifocal(double nu, double e, double p)
{
	double mul = sqrt(MU / p);

	return mul * arma::Col<double> { -sin(nu), e + cos(nu), 0 };
}

arma::Col<double> COE2RV(double sma, double ecc, double incl, double RAAN, double arg_lat, double aop, double true_aop, double true_anom, double true_long, double semiparameter)
{
	arma::Col<double> r_perifocal = { 0, 0, 0 };
	arma::Col<double> v_perifocal = { 0, 0, 0 };
	arma::Mat<double> A_perifocal_2_ECI = { { 0, 0, 0 }, { 0, 0, 0 }, { 0, 0, 0 } };

	if ((!std::isfinite(RAAN)) && (!std::isfinite(true_aop)))
	{
		r_perifocal = R_perifocal(true_long, ecc, semiparameter);
		v_perifocal = V_perifocal(true_long, ecc, semiparameter);
		A_perifocal_2_ECI = DCM_perifocal_to_ECI(0, incl, 0);
	}
	else if ((!std::isfinite(RAAN)) && (std::isfinite(true_aop)))
	{
		r_perifocal = R_perifocal(true_anom, ecc, semiparameter);
		v_perifocal = V_perifocal(true_anom, ecc, semiparameter);
		A_perifocal_2_ECI = DCM_perifocal_to_ECI(0, incl, true_aop);
	}
	else if ((std::isfinite(RAAN)) && (!std::isfinite(true_aop)))
	{
		r_perifocal = R_perifocal(arg_lat, ecc, semiparameter);
		v_perifocal = V_perifocal(arg_lat, ecc, semiparameter);
		A_perifocal_2_ECI = DCM_perifocal_to_ECI(RAAN, incl, 0);
	}
	else
	{
		r_perifocal = R_perifocal(true_anom, ecc, semiparameter);
		v_perifocal = V_perifocal(true_anom, ecc, semiparameter);
		A_perifocal_2_ECI = DCM_perifocal_to_ECI(RAAN, incl, aop);
	}

	return arma::join_cols(A_perifocal_2_ECI * r_perifocal, A_perifocal_2_ECI * v_perifocal);
}

arma::Col<double> move(arma::Col<double> R_ECI, arma::Col<double> V_ECI, double T, double n_dot_2, double n_dot_dot_6)
{
	arma::Col<double> COE_vec = RV2COE(R_ECI, V_ECI);

	double mean_anom = update_anomalies(COE_vec(1), COE_vec(7), COE_vec(4), COE_vec(8));

	COE_vec = update_for_perturbations(T, COE_vec, mean_anom, n_dot_2, n_dot_dot_6);

	arma::Col<double> anoms = update_anomalies_back(COE_vec(1), COE_vec(11), COE_vec(7), COE_vec(4), COE_vec(8));

	COE_vec(7) = anoms(0);
	COE_vec(4) = anoms(1);
	COE_vec(8) = anoms(2);

	return COE2RV(COE_vec(0), COE_vec(1), COE_vec(2), COE_vec(3), COE_vec(4), COE_vec(5), COE_vec(6), COE_vec(7), COE_vec(8), COE_vec(10));
}