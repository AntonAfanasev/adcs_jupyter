#include "SunSensors.h"

double SunSensors::get_sigma_sun()
{
	return sigma_sun;
}

void SunSensors::set_sigma_sun(double new_sigma_sun)
{
	sigma_sun = new_sigma_sun;
}

double SunSensors::get_ss_angle()
{
	return ss_angle;
}

void SunSensors::set_ss_angle(double new_ss_angle)
{
	ss_angle = new_ss_angle;
}

arma::Mat<double> SunSensors::get_normals()
{
	return normals;
}

void SunSensors::set_normals(arma::Mat<double> new_normals)
{
	normals = new_normals;
}

arma::Col<double> SunSensors::ss_measure(arma::Col<double> S_body_true)
{
	return arma::normalise(S_body_true + arma::randn(3) * sigma_sun);
}

bool SunSensors::inSight(arma::Col<double> S_body_model)
{
	bool iS1 = cos(arma::dot(arma::normalise(normals.row(0)), S_body_model)) >= cos(ss_angle);
	bool iS2 = cos(arma::dot(arma::normalise(normals.row(1)), S_body_model)) >= cos(ss_angle);
	bool iS3 = cos(arma::dot(arma::normalise(normals.row(2)), S_body_model)) >= cos(ss_angle);
	bool iS4 = cos(arma::dot(arma::normalise(normals.row(3)), S_body_model)) >= cos(ss_angle);
	bool iS5 = cos(arma::dot(arma::normalise(normals.row(4)), S_body_model)) >= cos(ss_angle);

	return (iS1 || iS2 || iS3 || iS4 || iS5);
}