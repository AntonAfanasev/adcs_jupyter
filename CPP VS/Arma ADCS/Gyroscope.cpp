#include "Gyroscope.h"

double Gyroscope::get_sigma_gyr()
{
	return sigma_gyr;
}

void  Gyroscope::set_sigma_gyr(double new_sigma_gyr)
{
	sigma_gyr = new_sigma_gyr;
}

arma::Col<double> Gyroscope::get_gyr_bias()
{
	return gyr_bias;
}

void Gyroscope::set_gyr_bias(arma::Col<double> new_gyr_bias)
{
	gyr_bias = new_gyr_bias;
}

arma::Col<double> Gyroscope::gyr_measure(arma::Col<double> omega)
{
	return omega + gyr_bias + arma::randn(3) * sigma_gyr;
}
