﻿// Arma ADCS.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <armadillo>
#include <cmath>
#include <iostream>
#include <iomanip>
#include "SunVector.h"
#include "IGRF.h"
#include "EKF.h"
#include "math_utils.h"
#include "propagation.h"
#include "sofa.h"
#include "sofam.h"
#include "orb_det.h"
#include "SGP4.h"

int main(int argc, const char** argv)
{
	// Initialize the random generator
	arma::arma_rng::set_seed_random();
	// Create a 4x4 random matrix and print it on the screen
	arma::Mat<double> A = arma::randu(4, 4);
	std::cout << "A:\n" << A << "\n";
	
	// Multiply A with his transpose:
	std::cout << "A * A.t() =\n";
	std::cout << A * A.t() << "\n";
	
	// Access/Modify rows and columns from the array:
	A.row(0) = A.row(1) + A.row(3);
	A.col(3).zeros();
	std::cout << "add rows 1 and 3, store result in row 0, also fill 4th column with zeros:\n";
	std::cout << "A:\n" << A << "\n";
	
	// Create a new diagonal matrix using the main diagonal of A:
	arma::Mat<double>B = arma::diagmat(A);
	std::cout << "B:\n" << B << "\n";
	
	// Save matrices A and B:
	A.save("A_mat.txt", arma::arma_ascii);
	B.save("B_mat.txt", arma::arma_ascii);

	EKF* ekf = new EKF;
	ekf->set_sigmas_init(1, arma::datum::pi / 10, 1e-7, 1e-9, 1e-9, 1e-5, 1e-9, 1e-9, 1e-5, 1e-9, 1e-9, 1e-5);
	ekf->set_P_init();
	std::cout << "P_corr:\n" << ekf->get_P_corr() << "\n";

	arma::Col<double> J_vec({ 0.011, 0.014, 0.009 });
	ekf->set_J_init(J_vec);
	std::cout << "G:\n" << ekf->get_G() << "\n";

	ekf->set_Sigma_init();
	std::cout << "Sigma_obs_MSG:\n" << ekf->get_Sigma_obs_MSG() << "\n";

	ekf->update_P_pred();
	std::cout << "P_pred:\n" << ekf->get_P_pred() << "\n";

	ekf->set_mean_motion(sqrt(3.986e+14 / pow(6371e+3 + 500e+3, 3)));
	std::cout << "mean motion:\n" << ekf->get_mean_motion() << "\n";

	arma::Col<double> vec1({1, 2, 3, 4});
	arma::Col<double> quat1 = arma::normalise(vec1);
	arma::Col<double> vec2({ -1, 4, 8 });
	arma::Col<double> rotation = rot_q(quat1, vec2);
	std::cout << "Check quaternion rotation:\n" << rotation << "\n";

	arma::Col<double> Omega = omega_to_Omega(arma::Col<double> ({ 0, 0, 0 }), quat1, ekf->get_mean_motion());
	std::cout << "Check Omega:\n" << Omega << "\n";

	SunVector* sv = new SunVector;
	sv->set_sigma_sun_vector(1e-9);
	std::cout << "Check std of sun model:\n" << sv->get_sigma_sun_vector() << "\n";

	arma::Col<double> s_model = sv->sun_vector_model(2458940.1933954097, -0.2);
	std::cout << "Check sun vector:\n" << s_model << "\n";

	arma::Col<double> rv = COE2RV(7178e3, 0, 98.6 * arma::datum::pi / 180, 300 * arma::datum::pi / 180, 0, arma::datum::nan, arma::datum::nan, arma::datum::nan, 300 * arma::datum::pi / 180, semi_from_sma_and_ecc(7178e3, 0));
	arma::Col<double> rv_new = move(rv.subvec(0, 2), rv.subvec(3, 5), 1, 0, 0);
	std::cout << "Check propagation:\n" << rv_new << "\n";

	double r_J2000[3] = { 1,0,0 };
	double v_J2000[3] = { 1,0,0 };
	double r_ITRS[3] = { 1,0,0 };
	double v_ITRS[3] = { 1,0,0 };
	
	J20002ITRS_RV_from_JD(2458940.1933954097, r_J2000, v_J2000, r_ITRS, v_ITRS);
	std::cout << "r_ITRS: " << r_ITRS[0] << " " << r_ITRS[1] << " " << r_ITRS[2] << "\n";

	ITRS2J2000_RV_from_JD(2458940.1933954097, r_ITRS, v_ITRS, r_J2000, v_J2000);
	std::cout << "r_J2000: " << r_J2000[0] << " " << r_J2000[1] << " " << r_J2000[2] << "\n";

	WGS842ITRS_R(DPI/3, DPI/4, 750000, r_ITRS);
	std::cout << "r_ITRS: " << r_ITRS[0] << " " << r_ITRS[1] << " " << r_ITRS[2] << "\n";

	double elong = 0;
	double phi = 0;
	double height = 0;

	ITRS2WGS84_R(r_ITRS, &elong, &phi, &height);
	std::cout << "geod: " << elong << " " << phi << " " << height << "\n";

	//-SGP4----------------------------------------------------------------------------------

	double r_radmin[3]; double v_radmin[3];
	double sec, jd, jdFrac, tsince, rteme[3], vteme[3], recef[3], vecef[3];
	double xincl, xnodeo, eo, omegao, xmo, xno;
	int  year, mon, day, hr, min;
	//char tle1[] = "1 08195U 75081A   06176.33215444  .00000099  00000-0  11873-3 0   813";
	//char tle2[] = "2 08195  64.1586 279.0717 6877146 264.7651  20.2257  2.00491383225656";
	char tle1[] = "1 23177U 94040C   06175.45752052  .00000386  00000-0  76590-3 0    95";
	char tle2[] = "2 23177   7.0496 179.8238 7258491 296.0482   8.3061  2.25906668 97438";
	char opsmode = 'i';
	gravconsttype whichconst = wgs84;
	elsetrec satrec;

	//-----------------------------------------------------------------------------------
	SGP4Funcs::sgp4_init_TLE(tle1, tle2, satrec);
	std::cout << "JD before: " << std::setprecision(9) << satrec.jdsatepoch << '\t' << satrec.jdsatepochF << '\n';

	SGP4Funcs::sgp4init(whichconst, opsmode, satrec.satnum, satrec.jdsatepoch + satrec.jdsatepochF - 2433281.5, satrec.bstar,
		satrec.ndot, satrec.nddot, satrec.ecco, satrec.argpo, satrec.inclo, satrec.mo, satrec.no_kozai,
		satrec.nodeo, satrec);

	tsince = 0; // time since epoch(minutes)
	//jd = 2453910.0;
	//jdFrac = 0.9582091784104705 ;
	SGP4Funcs::sgp4(satrec, tsince, rteme, vteme);
	std::cout << "RV_TEME: " << rteme[0] << ' ' << rteme[1] << ' ' << rteme[2] << ' ' << vteme[0] << ' ' << vteme[1] << ' ' << vteme[2] << '\n';

	rv2el(rteme, vteme, 0, tle1, tle2);
	std::cout << "TLE1: " << tle1 << '\n';
	std::cout << "TLE2: " << tle2 << '\n';

	arma::Col<double> mf_ECI = magn_field_ECI_from_spher(2458940.1933954097, 6371200, DPI / 3, DPI / 2);
	std::cout << "Magn Field:\t" << mf_ECI[0] << "\t" << mf_ECI[1] << "\t" << mf_ECI[2] << "\n";

	delete ekf;
	delete sv;
	return 0;
}
