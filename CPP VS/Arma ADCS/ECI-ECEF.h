#pragma once

#include <armadillo>

double DUT1_from_mjd(double MJD);
arma::Mat<double> DCM_ECEF_to_ECI(double JD_UTC);