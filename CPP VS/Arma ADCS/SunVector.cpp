#include "SunVector.h"

double SunVector::get_sigma_sun_vector()
{
	return sigma_sun_vector;
}

void SunVector::set_sigma_sun_vector(double new_sigma_sun_vector)
{
	sigma_sun_vector = new_sigma_sun_vector;
}

arma::Col<double> SunVector::sun_vector_model(double JD_UTC, double DUT1)
{
	double deg2rad = arma::datum::pi / 180;

	double JD_UT1 = JD_UTC + JD_add_seconds(DUT1);
	double UT1_centuries = julian_centuries(JD_UT1);

	double mean_longitude_Sun = 280.4606184 + 36000.77005361 * UT1_centuries;

//	int leap_seconds = 37;
//	double JD_TT = JD_UTC + JD_add_seconds(32.184 + leap_seconds);
//	double g = 357.53 + 0.9856003 * (JD_TT - 2451545);
//	g *= deg2rad;
//	double JD_TDB = JD_TT + JD_add_seconds(0.001658 * np.sin(g) + 0.000014 * np.sin(2 * g));
//
//	double TDB_centuries = julian_centuries(JD_TDB);

	double TDB_centuries = UT1_centuries;

	double mean_anomaly_Sun = 357.5277233 + 35999.05034 * TDB_centuries;
	mean_anomaly_Sun *= deg2rad;

	double ecliptic_longitude = mean_longitude_Sun + 1.914666471 * sin(mean_anomaly_Sun) + 0.019994643 * sin(2 * mean_anomaly_Sun);
	ecliptic_longitude *= deg2rad;

	double mean_obliquity_ecliptic = 23.439291 - 0.0130042 * TDB_centuries;
	mean_obliquity_ecliptic *= deg2rad;

	arma::Col<double> r_Earth_to_Sun{cos(ecliptic_longitude), cos(mean_obliquity_ecliptic) * sin(ecliptic_longitude), sin(mean_obliquity_ecliptic) * sin(ecliptic_longitude)};
	return r_Earth_to_Sun;
}

arma::Col<double> SunVector::sun_vector_true(arma::Col<double> S_model)
{
	arma::Col<double> d_Model = arma::randn(3) * sigma_sun_vector;

	return arma::normalise(S_model + d_Model);
}