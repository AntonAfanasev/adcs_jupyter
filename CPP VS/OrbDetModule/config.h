#pragma once

#define SMALL (0.00000001)
#define VERYSMALL (0.000000000001)
#define UNDEFINED (999999.1)
#define INFINITE (999999.9)

/* TAI - UTC (s) */
#define DAT (37)

/* degree of truncation (for scalar potential series expansion) */
/* source: https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html */
/* CAN NOT BE GREATER THAN 13 */
#define N_IGRF (13)