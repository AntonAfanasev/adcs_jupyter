#include "math_utils.h"

// converts DCM into quaternion
// source: https://www.astro.rug.nl/software/kapteyn-beta/_downloads/attitude.pdf p.15
// inputs:
//		DCM -- Directional Cosine Matrix
// outputs:
//		quat -- quaternion
void DCM2QUAT(double DCM[3][3], double quat[4])
{
	// if (A22 > -A33) and (A11 > -A22) and (A11 > -A33)
	if ((DCM[1][1] > -1 * DCM[2][2]) && (DCM[0][0] > -1 * DCM[1][1]) && (DCM[0][0] > -1 * DCM[2][2]))
	{
		// s = sqrt(1 + A11 + A22 + A33)
		// q = 0.5 / s * [s^2, A23 - A32, A31 - A13, A12 - A21]
		double s = sqrt(1 + DCM[0][0] + DCM[1][1] + DCM[2][2]);
		quat[0] = 0.5 * s;
		quat[1] = 0.5 * (DCM[1][2] - DCM[2][1]) / s;
		quat[2] = 0.5 * (DCM[2][0] - DCM[0][2]) / s;
		quat[3] = 0.5 * (DCM[0][1] - DCM[1][0]) / s;
	}
	// if (A22 < -A33) and (A11 > A22) and (A11 > A33)
	else if ((DCM[1][1] < -1 * DCM[2][2]) && (DCM[0][0] > DCM[1][1]) && (DCM[0][0] > DCM[2][2]))
	{
		// s = sqrt(1 + A11 - A22 - A33)
		// q = 0.5 / s * [A23 - A32, s^2, A12 + A21, A31 + A13]
		double s = sqrt(1 + DCM[0][0] - DCM[1][1] - DCM[2][2]);
		quat[0] = 0.5 * (DCM[1][2] - DCM[2][1]) / s;
		quat[1] = 0.5 * s;
		quat[2] = 0.5 * (DCM[0][1] + DCM[1][0]) / s;
		quat[3] = 0.5 * (DCM[2][0] + DCM[0][2]) / s;
	}
	// if (A22 > A33) and (A11 < A22) and (A11 < -A33)
	else if ((DCM[1][1] > DCM[2][2]) && (DCM[0][0] < DCM[1][1]) && (DCM[0][0] < -1 * DCM[2][2]))
	{
		// s = sqrt(1 - A11 + A22 - A33)
		// q = 0.5 / s * [A31 - A13, A12 + A21, s^2, A23 + A32]
		double s = sqrt(1 - DCM[0][0] + DCM[1][1] - DCM[2][2]);
		quat[0] = 0.5 * (DCM[2][0] - DCM[0][2]) / s;
		quat[1] = 0.5 * (DCM[0][1] + DCM[1][0]) / s;
		quat[2] = 0.5 * s;
		quat[3] = 0.5 * (DCM[1][2] + DCM[2][1]) / s;
	}
	// if (A22 < A33) and (A11 < -A22) and (A11 < A33)
	else if ((DCM[1][1] < DCM[2][2]) && (DCM[0][0] < -1 * DCM[1][1]) && (DCM[0][0] < DCM[2][2]))
	{
		// s = sqrt(1 - A11 - A22 + A33)
		// q = 0.5 / s * [A12 - A21, A31 + A13, A23 + A32, s^2]
		double s = sqrt(1 - DCM[0][0] - DCM[1][1] + DCM[2][2]);
		quat[0] = 0.5 * (DCM[0][1] - DCM[1][0]) / s;
		quat[1] = 0.5 * (DCM[2][0] + DCM[0][2]) / s;
		quat[2] = 0.5 * (DCM[1][2] + DCM[2][1]) / s;
		quat[3] = 0.5 * s;
	}
	// in case of miracle
	else
	{
		// q = [1, 0, 0, 0]
		quat[0] = 1;
		quat[1] = 0;
		quat[2] = 0;
		quat[3] = 0;
	}
}

// converts quaternion into DCM
// source: https://www.astro.rug.nl/software/kapteyn-beta/_downloads/attitude.pdf p.15
// inputs:
//		quat -- quaternion
// outputs:
//		DCM -- Directional Cosine Matrix
void QUAT2DCM(double quat[4], double DCM[3][3])
{
	// A11 = q0^2 + q1^2 - q2^2 - q3^2
	// A12 = 2 * (q1 * q2 + q0 * q3)
	// A13 = 2 * (q1 * q3 - q0 * q2)
	DCM[0][0] = pow(quat[0], 2) + pow(quat[1], 2) - pow(quat[2], 2) - pow(quat[3], 2);
	DCM[0][1] = 2 * quat[1] * quat[2] + 2 * quat[0] * quat[3];
	DCM[0][2] = 2 * quat[1] * quat[3] - 2 * quat[0] * quat[2];

	// A21 = 2 * (q1 * q2 - q0 * q3)
	// A22 = q0^2 - q1^2 + q2^2 - q3^2
	// A23 = 2 * (q2 * q3 + q0 * q1)
	DCM[1][0] = 2 * quat[1] * quat[2] - 2 * quat[0] * quat[3];
	DCM[1][1] = pow(quat[0], 2) - pow(quat[1], 2) + pow(quat[2], 2) - pow(quat[3], 2);
	DCM[1][2] = 2 * quat[2] * quat[3] + 2 * quat[0] * quat[1];

	// A31 = 2 * (q1 * q3 + q0 * q2)
	// A32 = 2 * (q2 * q3 - q0 * q1)
	// A33 = q0^2 - q1^2 - q2^2 + q3^2
	DCM[2][0] = 2 * quat[1] * quat[3] + 2 * quat[0] * quat[2];
	DCM[2][1] = 2 * quat[2] * quat[3] - 2 * quat[0] * quat[1];
	DCM[2][2] = pow(quat[0], 2) - pow(quat[1], 2) - pow(quat[2], 2) + pow(quat[3], 2);
}

// calculates arccosine
// if abs(x) > 1, returns the closest possible angle -- 0 or pi
// inputs:
//		x -- cosine value
// returns:
//		arccosine of x
double acose(double x)
{
	double rval;

	// if abs(x) >= 1, return closest possible angle -- 0 or pi
	if (x >= 1.) rval = 0.;
	else if (x <= -1.) rval = DPI;
	// if x is ok, return common arccosine
	else rval = acos(x);
	return(rval);
}

/* -----------------------------------------------------------------------------
	*
	*                           procedure angle
	*
	*  this procedure calculates the angle between two vectors.  the output is
	*    set to UNDEFINED to indicate an undefined value.  be sure to check for
	*    this at the output phase.
	*
	*  author        : david vallado                  719-573-2600    1 mar 2001
	*
	*  inputs          description                    range / units
	*    vec1        - vector number 1
	*    vec2        - vector number 2
	*
	*  outputs       :
	*    theta       - angle between the two vectors  -pi to pi
	*
	*  locals        :
	*    temp        - temporary real variable
	*
	*  coupling      :
	*    dot           dot product of two vectors
	* --------------------------------------------------------------------------- */
double angle(double vec1[3], double vec2[3])
{
	double magv1, magv2, temp;

	// calculate magnitudes of vectors
	magv1 = iauPm(vec1);
	magv2 = iauPm(vec2);

	// if magnitudes are too small, we have a possibility to divide by zero
	// in this case return UNDEFINED
	if (magv1 * magv2 > SMALL * SMALL)
	{
		// if magnitudes are big enough, calculate cosine of the angle between given vectors
		temp = iauPdp(vec1, vec2) / (magv1 * magv2);
		// and return its arccosine
		return acose(temp);
	}
	else
		return UNDEFINED;
}

// normalize the angle into the interval [0, 2 pi]
// inputs:
//		x -- not normalized angle
// returns:
//		normalized angle
double fmod2p(const double x)
{
	// calculate residue
	double rval = fmod(x, D2PI);
	// if it's negative, add one period
	if (rval < 0.)
		rval += D2PI;
	return(rval);
}