#pragma once

#include "sofa.h"
#include "sofam.h"
#include "SGP4.h"
#include "config.h"
#include "math_utils.h"
#include "IGRF.h"
#include <cstring>
#include <stdint.h>

/* angular velocity of the Earth [rad/s] */
#define OMEGA_EARTH (2 * DPI / (DAYSEC - 236))
/* 2433282.5 -- Julian day of 0h January 1, 1950 */
#define JD_SGP4_REF (2433282.5)
/* standard gravitational parameter of the Earth for WGS84 [km^3/s^2] */
#define MU_WGS84 (398600.5)
/* semi-major axis for WGS84 [m] */
#define SMA_WGS84 (6378137)
/* semi-minor axis for WGS84 [m] */
#define SMI_WGS84 (6356752.3142)
/* first eccentricity squared for WGS84 */
#define E2_WGS84 (0.00669437999014)
/* b^2/a^2 for WGS84 */
#define B2_A2_WGS84 (1 - E2_WGS84)

//-----------------------------------------------------------  time.cpp  ----------------------------------------------------------------------

double julian2days(double JD);
void days2mdhms(int year, double days, int& mon, int& day, int& hr, int& minute, double& sec);

//---------------------------------------------------------  bulletins.cpp  -------------------------------------------------------------------

double get_DUT1(double JD_UTC);
void get_TIP_coords(double JD_UTC, double* xp, double* yp);
void get_DUT1_and_TIP_coords(double JD_UTC, double* dut1, double* xp, double* yp);

//---------------------------------------------------------  ref_frames.cpp  ------------------------------------------------------------------

//----  block for ECI-ECEF conversions  -------------------------------------------------------------------------------------------------------

void J20002ITRS_DCM(double JD_UTC, double DCM_J2000_to_ITRS[3][3]);
void J20002ITRS_RV_from_JD(double JD_UTC, double r_J2000[3], double v_J2000[3], double r_ITRS[3], double v_ITRS[3]);
void J20002ITRS_RV_from_DCM(double DCM_J2000_to_ITRS[3][3], double r_J2000[3], double v_J2000[3], double r_ITRS[3], double v_ITRS[3]);

void ITRS2J2000_DCM(double JD_UTC, double DCM_ITRS_to_J2000[3][3]);
void ITRS2J2000_RV_from_JD(double JD_UTC, double r_ITRS[3], double v_ITRS[3], double r_J2000[3], double v_J2000[3]);
void ITRS2J2000_RV_from_DCM(double DCM_ITRS_to_J2000[3][3], double r_ITRS[3], double v_ITRS[3], double r_J2000[3], double v_J2000[3]);

//----  block for LLA-ECEF conversions  -------------------------------------------------------------------------------------------------------

void WGS842ITRS_R(double elong, double lat_gd, double height_gd, double r_ITRS[3]);
void ITRS2WGS84_R(double r_ITRS[3], double* elong, double* lat_gd, double* height_gd);

void WGS842ITRS_DCM(double elong, double lat_gd, double DCM_WGS84_to_ITRS[3][3]);
void WGS842ITRS_V(double v_WGS84[3], double elong, double lat_gd, double v_ITRS[3]);
void ITRS2WGS84_DCM(double elong, double lat_gd, double DCM_ITRS_to_WGS84[3][3]);
void ITRS2WGS84_V(double v_ITRS[3], double elong, double lat_gd, double v_WGS84[3]);

//----  block for SEU-ECEF conversions  -------------------------------------------------------------------------------------------------------

void SEU2ITRS_DCM(double colat_gc, double elong, double DCM_SEU_to_ITRS[3][3]);
void SEU2ITRS_vec(double vec_SEU[3], double colat_gc, double elong, double vec_ITRS[3]);
void ITRS2SEU_DCM(double colat_gc, double elong, double DCM_ITRS_to_SEU[3][3]);
void ITRS2SEU_vec(double vec_ITRS[3], double colat_gc, double elong, double vec_SEU[3]);

//----  block for SEU-LLA conversions  --------------------------------------------------------------------------------------------------------

void GD2SEU(double lat_gd, double height_gd, double* colat_gc, double* rad_gc);
void SEU2GD(double colat_gc, double rad_gc, double* lat_gd, double* height_gd);

//----  block for ECI-LVLH conversions  -------------------------------------------------------------------------------------------------------
// LVLH_Z is co-directional to R
// LVLH_Y is co-directional to RxV
// LVLH_X is co-directional to Rx[RxV]

void J20002LVLH_DCM(double r_J2000[3], double v_J2000[3], double DCM_J2000_to_LVLH[3][3]);
void J20002LVLH_Q(double r_J2000[3], double v_J2000[3], double Q_J2000_to_LVLH[4]);
void J20002LVLH_RV(double r_J2000[3], double v_J2000[3], double r_LVLH[3], double v_LVLH[3]);

//----  block for TEME-ECEF conversions  ------------------------------------------------------------------------------------------------------

void TEME2ITRS_DCM(double JD_UTC, double DCM_TEME_to_ITRS[3][3]);
void TEME2ITRS_RV_from_JD(double JD_UTC, double r_TEME[3], double v_TEME[3], double r_ITRS[3], double v_ITRS[3]);
void TEME2ITRS_RV_from_DCM(double DCM_TEME_to_ITRS[3][3], double r_TEME[3], double v_TEME[3], double r_ITRS[3], double v_ITRS[3]);

void ITRS2TEME_DCM(double JD_UTC, double DCM_ITRS_to_TEME[3][3]);
void ITRS2TEME_RV_from_JD(double JD_UTC, double r_ITRS[3], double v_ITRS[3], double r_TEME[3], double v_TEME[3]);
void ITRS2TEME_RV_from_DCM(double DCM_ITRS_to_TEME[3][3], double r_ITRS[3], double v_ITRS[3], double r_TEME[3], double v_TEME[3]);

//-------------------------------------------------------  rvTEME_to_TLE.cpp  -----------------------------------------------------------------

void rv2el(double* rr, double* vv, char* tle1, char* tle2);

//-----------------------------------------------------------  orb_det.cpp  -------------------------------------------------------------------

// enumeration of possible reference frames
typedef enum RefFrame
{
	TEME, // default TEME
	ECEF, // geocentric ITRS
	ECI, // inertial J2000
	LLA, // geodetic WGS84
	LVLH // orbital
} RefFrame;

// GPS data structure
typedef struct
{
	uint32_t stat; // GPS status, irrelevant
	float lat; // [rad], latitude
	float lon; // [rad], east longitude
	float alt; // [m], altitude
	float vel; // [m/s], planar component of velocity (LLA)
	float azim; // [rad], velocity's azimuth
	float lift; // [m/s], vertical component of velocity (LLA)
	uint32_t rsvd; // reserved space, irrelevant
	int32_t gps_mark; // [s] starting from 0h Jun 1, 1970, UTC without leap seconds
	int64_t t0; // [ns] starting from 0h Jun 1, 1970, UTC without leap seconds

} gps_data_time_s;

void update_TLE_from_NORAD(char* tle1, char* tle2);
int update_TLE_from_GPS(gps_data_time_s* gps_current);
void get_RV(RefFrame rf, double JD_UTC, double R[3], double V[3]);