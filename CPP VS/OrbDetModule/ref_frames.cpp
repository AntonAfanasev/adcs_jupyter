#include "orb_det.h"

// block for ECEF-ECI conversions
// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.106

// compute DCM for reference frames conversion: ECI -> ECEF for given Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
// outputs:
//		DCM_J2000_to_ITRS -- Directional Cosine Matrix for ECI -> ECEF conversion, -1 <= doubles <= 1
void J20002ITRS_DCM(double JD_UTC, double DCM_J2000_to_ITRS[3][3])
{
	int flag = 0;
	double ut11 = 0, ut12 = 0, tt1 = 0, tt2 = 0, dut1 = 0, xp = 0, yp = 0;

	// dut1 = UT1-UTC [s] for the current day
	// xp, yp -- coordinates of the pole in ITRS in radians
	get_DUT1_and_TIP_coords(JD_UTC, &dut1, &xp, &yp);

	// dt = TT-UT1 [s]
	double dt = TTMTAI + DAT - dut1;
	// ut11 + ut12 = ut1 -- current UT1 time [JD]
	// flag -- error status (should be 0)
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.354
	flag = iauUtcut1(JD_UTC, 0, dut1, &ut11, &ut12);
	// tt1 + tt2 = tt -- current TT time [JD]
	// flag -- error status (should be 0)
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.351
	flag = iauUt1tt(ut11, ut12, dt, &tt1, &tt2);

	// function, which calculates DCM_J2000_to_ITRS from current time and poles' coordinates
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.106
	iauC2t06a(tt1, tt2, ut11, ut12, xp, yp, DCM_J2000_to_ITRS);
}

// compute position and velocity of satellite in ECEF frame, given position and velocity in ECI frame and current Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
//		r_J2000 -- [any] satellite's position in ECI, doubles
//		v_J2000 -- [any]/[s] satellite's velocity in ECI, doubles
// outputs:
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
void J20002ITRS_RV_from_JD(double JD_UTC, double r_J2000[3], double v_J2000[3], double r_ITRS[3], double v_ITRS[3])
{
	double DCM_J2000_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} }, v_ITRS_rel[3] = { 0,0,0 }, v_ITRS_fr[3] = { 0,0,0 };

	// compute DCM_J2000_to_ITRS for the given date
	J20002ITRS_DCM(JD_UTC, DCM_J2000_to_ITRS);

	// r_ITRS = DCM_J2000_to_ITRS * r_J2000
	iauRxp(DCM_J2000_to_ITRS, r_J2000, r_ITRS);
	// v_ITRS_rel = DCM_J2000_to_ITRS * v_J2000
	// v_ITRS_rel -- relative velocity
	iauRxp(DCM_J2000_to_ITRS, v_J2000, v_ITRS_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_ITRS_fr = [Omega_Earth x r_ITRS]
	// v_ITRS_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_ITRS, v_ITRS_fr);

	// v_ITRS = v_ITRS_rel - v_ITRS_fr
	// v_ITRS = A * v_J2000 - [Omega_Earth x r_ITRS]
	iauPmp(v_ITRS_rel, v_ITRS_fr, v_ITRS);
}

// compute position and velocity of satellite in ECEF frame, given position and velocity in ECI frame and DCM for conversion ECI -> ECEF
// inputs:
//		DCM_J2000_to_ITRS -- Directional Cosine Matrix for ECI -> ECEF conversion, -1 <= doubles <= 1
//		r_J2000 -- [any] satellite's position in ECI, doubles
//		v_J2000 -- [any]/[s] satellite's velocity in ECI, doubles
// outputs:
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
void J20002ITRS_RV_from_DCM(double DCM_J2000_to_ITRS[3][3], double r_J2000[3], double v_J2000[3], double r_ITRS[3], double v_ITRS[3])
{
	double v_ITRS_rel[3] = { 0,0,0 }, v_ITRS_fr[3] = { 0,0,0 };
	
	// r_ITRS = DCM_J2000_to_ITRS * r_J2000
	iauRxp(DCM_J2000_to_ITRS, r_J2000, r_ITRS);
	// v_ITRS_rel = DCM_J2000_to_ITRS * v_J2000
	// v_ITRS_rel -- relative velocity
	iauRxp(DCM_J2000_to_ITRS, v_J2000, v_ITRS_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_ITRS_fr = [Omega_Earth x r_ITRS]
	// v_ITRS_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_ITRS, v_ITRS_fr);

	// v_ITRS = v_ITRS_rel - v_ITRS_fr
	// v_ITRS = A * v_J2000 - [Omega_Earth x r_ITRS]
	iauPmp(v_ITRS_rel, v_ITRS_fr, v_ITRS);
}

// compute DCM for reference frames conversion: ECEF -> ECI for given Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
// outputs:
//		DCM_ITRS_to_J2000 -- Directional Cosine Matrix for ECEF -> ECI conversion, -1 <= doubles <= 1
void ITRS2J2000_DCM(double JD_UTC, double DCM_ITRS_to_J2000[3][3])
{
	// compute DCM_J2000_to_ITRS for the given date
	double DCM_J2000_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	J20002ITRS_DCM(JD_UTC, DCM_J2000_to_ITRS);

	// transpose DCM_J2000_to_ITRS to get DCM_ITRS_to_J2000
	// DCM_ITRS_to_J2000 = (DCM_J2000_to_ITRS).T
	iauTr(DCM_J2000_to_ITRS, DCM_ITRS_to_J2000);
}

// compute position and velocity of satellite in ECI frame, given position and velocity in ECEF frame and current Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
// outputs:
//		r_J2000 -- [any] satellite's position in ECI, doubles
//		v_J2000 -- [any]/[s] satellite's velocity in ECI, doubles
void ITRS2J2000_RV_from_JD(double JD_UTC, double r_ITRS[3], double v_ITRS[3], double r_J2000[3], double v_J2000[3])
{
	double DCM_ITRS_to_J2000[3][3] = { {1,0,0},{0,1,0},{0,0,1} }, v_J2000_rel[3] = { 0,0,0 }, v_J2000_fr[3] = { 0,0,0 };

	// compute DCM_ITRS_to_J2000 for the given date
	ITRS2J2000_DCM(JD_UTC, DCM_ITRS_to_J2000);

	// r_J2000 = DCM_ITRS_to_J2000 * r_ITRS
	iauRxp(DCM_ITRS_to_J2000, r_ITRS, r_J2000);
	// v_J2000_rel = DCM_ITRS_to_J2000 * v_ITRS
	// v_J2000_rel -- relative velocity
	iauRxp(DCM_ITRS_to_J2000, v_ITRS, v_J2000_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_J2000_fr = [Omega_Earth x r_J2000]
	// v_J2000_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_J2000, v_J2000_fr);

	// v_J2000 = v_J2000_rel + v_J2000_fr
	// v_J2000 = A.T * v_ITRS + [Omega_Earth x r_J2000]
	iauPpp(v_J2000_rel, v_J2000_fr, v_J2000);
}

// compute position and velocity of satellite in ECI frame, given position and velocity in ECEF frame and DCM for conversion ECEF -> ECI
// inputs:
//		DCM_ITRS_to_J2000 -- Directional Cosine Matrix for ECEF -> ECI conversion, -1 <= doubles <= 1
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
// outputs:
//		r_J2000 -- [any] satellite's position in ECI, doubles
//		v_J2000 -- [any]/[s] satellite's velocity in ECI, doubles
void ITRS2J2000_RV_from_DCM(double DCM_ITRS_to_J2000[3][3], double r_ITRS[3], double v_ITRS[3], double r_J2000[3], double v_J2000[3])
{
	double v_J2000_rel[3] = { 0,0,0 }, v_J2000_fr[3] = { 0,0,0 };

	// r_J2000 = DCM_ITRS_to_J2000 * r_ITRS
	iauRxp(DCM_ITRS_to_J2000, r_ITRS, r_J2000);
	// v_J2000_rel = DCM_ITRS_to_J2000 * v_ITRS
	// v_J2000_rel -- relative velocity
	iauRxp(DCM_ITRS_to_J2000, v_ITRS, v_J2000_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_J2000_fr = [Omega_Earth x r_J2000]
	// v_J2000_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_J2000, v_J2000_fr);

	// v_J2000 = v_J2000_rel + v_J2000_fr
	// v_J2000 = A.T * v_ITRS + [Omega_Earth x r_J2000]
	iauPpp(v_J2000_rel, v_J2000_fr, v_J2000);
}

// block for geodetic-ECEF conversions

// compute position of satellite in ECEF frame, given position in LLA frame
// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.179
// inputs:
//		elong -- [rad] geodetic eastern longitude, double from -pi to pi
//		lat_gd -- [rad] geodetic latitude, double from -pi/2 to pi/2
//		height_gd -- [m] geodetic altitude, positive double
// outputs:
//		r_ITRS -- [m] satellite's position in ECEF, doubles
void WGS842ITRS_R(double elong, double lat_gd, double height_gd, double r_ITRS[3])
{
	int flag = 0;

	// function, which calculates r_ITRS from LLA coordinates
	// we use WGS84 parameters
	// flag -- error status (should be 0)
	flag = iauGd2gc(WGS84, elong, lat_gd, height_gd, r_ITRS);
}

// compute position of satellite in LLA frame, given position in ECEF frame
// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.177
// inputs:
//		r_ITRS -- [m] satellite's position in ECEF, doubles
// outputs:
//		elong -- [rad] geodetic eastern longitude (address of the required variable), double from -pi to pi
//		lat_gd -- [rad] geodetic latitude (address of the required variable), double from -pi/2 to pi/2
//		height_gd -- [m] geodetic altitude (address of the required variable), positive double
void ITRS2WGS84_R(double r_ITRS[3], double *elong, double *lat_gd, double *height_gd)
{
	int flag = 0;

	// function, which calculates LLA coordinates from r_ITRS
	// we use WGS84 parameters
	// flag -- error status (should be 0)
	flag = iauGc2gd(WGS84, r_ITRS, elong, lat_gd, height_gd);
}

// compute DCM for reference frames conversion: LLA -> ECEF for given position in LLA frame
// source: https://gssc.esa.int/navipedia/index.php/Transformations_between_ECEF_and_ENU_coordinates
// inputs:
//		elong -- [rad] geodetic eastern longitude, double from -pi to pi
//		lat_gd -- [rad] geodetic latitude, double from -pi/2 to pi/2
// outputs:
//		DCM_WGS84_to_ITRS -- Directional Cosine Matrix for LLA -> ECEF conversion, -1 <= doubles <= 1
void WGS842ITRS_DCM(double elong, double lat_gd, double DCM_WGS84_to_ITRS[3][3])
{
	// cosines and sines of geodetic parameters, frequently used further
	double cos_phi    = cos(lat_gd);
	double sin_phi    = sin(lat_gd);
	double cos_lambda = cos(elong);
	double sin_lambda = sin(elong);

	// v_ITRS = DCM * v_WGS84
	// DCM = R3(-(pi/2 + lambda)) * R1(-(pi/2 - phi))
	//		 [-sin(lambda)	-cos(lambda)*sin(phi)	cos(lambda)*cos(phi)]
	// DCM = [ cos(lambda)	-sin(lambda)*sin(phi)	sin(lambda)*cos(phi)]
	//		 [		     0				 cos(phi)				sin(phi)]
	DCM_WGS84_to_ITRS[0][0] = -sin_lambda;
	DCM_WGS84_to_ITRS[0][1] = -cos_lambda * sin_phi;
	DCM_WGS84_to_ITRS[0][2] =  cos_lambda * cos_phi;
	
	DCM_WGS84_to_ITRS[1][0] =  cos_lambda;
	DCM_WGS84_to_ITRS[1][1] = -sin_lambda * sin_phi;
	DCM_WGS84_to_ITRS[1][2] =  sin_lambda * cos_phi;
	
	DCM_WGS84_to_ITRS[2][0] =					  0;
	DCM_WGS84_to_ITRS[2][1] =				cos_phi;
	DCM_WGS84_to_ITRS[2][2] =				sin_phi;
}

// compute velocity of satellite in ECEF frame, given velocity and position in LLA frame
// inputs:
//		v_WGS84 -- [any] satellite's velocity in LLA, v_WGS84 = { East, North, Up }, doubles
//		elong -- [rad] geodetic eastern longitude, double from -pi to pi
//		lat_gd -- [rad] geodetic latitude, double from -pi/2 to pi/2
// outputs:
//		v_ITRS -- [any] satellite's velocity in ECEF, doubles
void WGS842ITRS_V(double v_WGS84[3], double elong, double lat_gd, double v_ITRS[3])
{
	// initialize DCM for LLA -> ECEF conversion
	double DCM_WGS84_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	WGS842ITRS_DCM(elong, lat_gd, DCM_WGS84_to_ITRS);

	// v_ITRS = DCM_WGS84_to_ITRS * v_WGS84
	iauRxp(DCM_WGS84_to_ITRS, v_WGS84, v_ITRS);
}

// compute DCM for reference frames conversion: ECEF -> LLA for given position in LLA frame
// source: https://gssc.esa.int/navipedia/index.php/Transformations_between_ECEF_and_ENU_coordinates
// inputs:
//		elong -- [rad] geodetic eastern longitude, double from -pi to pi
//		lat_gd -- [rad] geodetic latitude, double from -pi/2 to pi/2
// outputs:
//		DCM_ITRS_to_WGS84 -- Directional Cosine Matrix for ECEF -> LLA conversion, -1 <= doubles <= 1
void ITRS2WGS84_DCM(double elong, double lat_gd, double DCM_ITRS_to_WGS84[3][3])
{
	// cosines and sines of geodetic parameters, frequently used further
	double cos_phi    = cos(lat_gd);
	double sin_phi    = sin(lat_gd);
	double cos_lambda = cos(elong);
	double sin_lambda = sin(elong);

	// v_WGS84 = DCM * v_ITRS
	// DCM = R1(pi/2 - phi) * R3(pi/2 + lambda)
	//		 [-sin(lambda)				cos(lambda)					   0]
	// DCM = [-cos(lambda)*sin(phi)	   -sin(lambda)*sin(phi)	cos(phi)]
	//		 [ cos(lambda)*cos(phi)		sin(lambda)*cos(phi)	sin(phi)]
	DCM_ITRS_to_WGS84[0][0] = -sin_lambda;
	DCM_ITRS_to_WGS84[0][1] =  cos_lambda;
	DCM_ITRS_to_WGS84[0][2] =			0;
	
	DCM_ITRS_to_WGS84[1][0] = -cos_lambda * sin_phi;
	DCM_ITRS_to_WGS84[1][1] = -sin_lambda * sin_phi;
	DCM_ITRS_to_WGS84[1][2] =				cos_phi;
	
	DCM_ITRS_to_WGS84[2][0] =  cos_lambda * cos_phi;
	DCM_ITRS_to_WGS84[2][1] =  sin_lambda * cos_phi;
	DCM_ITRS_to_WGS84[2][2] =				sin_phi;
}

// compute velocity of satellite in LLA frame, given velocity in ECEF frame and position in LLA frame
// inputs:
//		v_ITRS -- [any] satellite's velocity in ECEF, doubles
//		elong -- [rad] geodetic eastern longitude, double from -pi to pi
//		lat_gd -- [rad] geodetic latitude, double from -pi/2 to pi/2
// outputs:
//		v_WGS84 -- [any] satellite's velocity in LLA, v_WGS84 = { East, North, Up }, doubles
void ITRS2WGS84_V(double v_ITRS[3], double elong, double lat_gd, double v_WGS84[3])
{
	// initialize DCM for ECEF -> LLA conversion
	double DCM_ITRS_to_WGS84[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	ITRS2WGS84_DCM(elong, lat_gd, DCM_ITRS_to_WGS84);

	// v_WGS84 = DCM_ITRS_to_WGS84 * v_ITRS
	iauRxp(DCM_ITRS_to_WGS84, v_ITRS, v_WGS84);
}

// block for SEU-ECEF conversions

// compute DCM for reference frames conversion: SEU -> ECEF for given position in SEU frame
// source: manual matrix composition
// inputs:
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// outputs:
//		DCM_SEU_to_ITRS -- Directional Cosine Matrix for SEU -> ECEF conversion, -1 <= doubles <= 1
void SEU2ITRS_DCM(double colat_gc, double elong, double DCM_SEU_to_ITRS[3][3])
{
	// cosines and sines of geocentric parameters, frequently used further
	double cos_theta  = cos(colat_gc);
	double sin_theta  = sin(colat_gc);
	double cos_lambda = cos(elong);
	double sin_lambda = sin(elong);

	// DCM = R3(lambda) * R2(theta)
	//		 [cos(lambda)*cos(theta)	-sin(lambda)	cos(lambda)*sin(theta)]
	// DCM = [sin(lambda)*cos(theta)	 cos(lambda)	sin(lambda)*sin(theta)]
	//		 [			 -sin(theta)			   0	   			cos(theta)]
	DCM_SEU_to_ITRS[0][0] =  cos_lambda * cos_theta;
	DCM_SEU_to_ITRS[0][1] = -sin_lambda;
	DCM_SEU_to_ITRS[0][2] =  cos_lambda * sin_theta;

	DCM_SEU_to_ITRS[1][0] =  sin_lambda * cos_theta;
	DCM_SEU_to_ITRS[1][1] =  cos_lambda;
	DCM_SEU_to_ITRS[1][2] =  sin_lambda * sin_theta;

	DCM_SEU_to_ITRS[2][0] =				 -sin_theta;
	DCM_SEU_to_ITRS[2][1] =						  0;
	DCM_SEU_to_ITRS[2][2] =				  cos_theta;
}

// compute vector in ECEF frame, given same vector in SEU frame and satellite's position in SEU frame
// inputs:
//		vec_SEU -- [any] vector in SEU, doubles
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// outputs:
//		vec_ITRS -- [any] vector in ECEF, doubles
void SEU2ITRS_vec(double vec_SEU[3], double colat_gc, double elong, double vec_ITRS[3])
{
	// initialize DCM for SEU -> ECEF conversion
	double DCM_SEU_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	SEU2ITRS_DCM(colat_gc, elong, DCM_SEU_to_ITRS);

	// vec_ITRS = DCM_SEU_to_ITRS * vec_SEU
	iauRxp(DCM_SEU_to_ITRS, vec_SEU, vec_ITRS);
}

// compute DCM for reference frames conversion: ECEF -> SEU for given position in SEU frame
// source: manual matrix composition
// inputs:
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// outputs:
//		DCM_ITRS_to_SEU -- Directional Cosine Matrix for ECEF -> SEU conversion, -1 <= doubles <= 1
void ITRS2SEU_DCM(double colat_gc, double elong, double DCM_ITRS_to_SEU[3][3])
{
	// cosines and sines of geocentric parameters, frequently used further
	double cos_theta  = cos(colat_gc);
	double sin_theta  = sin(colat_gc);
	double cos_lambda = cos(elong);
	double sin_lambda = sin(elong);

	// DCM = R2(-theta) * R3(-lambda)
	//		 [ cos(lambda)*cos(theta)	sin(lambda)*cos(theta)	-sin(theta)]
	// DCM = [-sin(lambda)				cos(lambda)						  0]
	//		 [ cos(lambda)*sin(theta)	sin(lambda)*sin(theta)	 cos(theta)]
	DCM_ITRS_to_SEU[0][0] =  cos_lambda * cos_theta;
	DCM_ITRS_to_SEU[0][1] =  sin_lambda * cos_theta;
	DCM_ITRS_to_SEU[0][2] =				 -sin_theta;

	DCM_ITRS_to_SEU[1][0] = -sin_lambda;
	DCM_ITRS_to_SEU[1][1] =  cos_lambda;
	DCM_ITRS_to_SEU[1][2] =  0;

	DCM_ITRS_to_SEU[2][0] =  cos_lambda * sin_theta;
	DCM_ITRS_to_SEU[2][1] =  sin_lambda * sin_theta;
	DCM_ITRS_to_SEU[2][2] =				  cos_theta;
}

// compute vector in SEU frame, given same vector in ECEF frame and satellite's position in SEU frame
// inputs:
//		vec_ITRS -- [any] vector in ECEF, doubles
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// outputs:
//		vec_SEU -- [any] vector in SEU, doubles
void ITRS2SEU_vec(double vec_ITRS[3], double colat_gc, double elong, double vec_SEU[3])
{
	// initialize DCM for ECEF -> SEU conversion
	double DCM_ITRS_to_SEU[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	ITRS2SEU_DCM(colat_gc, elong, DCM_ITRS_to_SEU);

	// vec_SEU = DCM_ITRS_to_SEU * vec_ITRS
	iauRxp(DCM_ITRS_to_SEU, vec_ITRS, vec_SEU);
}

// block for SEU-geodetic conversions

// convert geodetic parameters in LLA frame (EastNorthUp) to geocentric parameters in SEU (SouthEastUp)
// eastern longitude is identical in both frames
// source: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion
// inputs:
//		lat_gd -- [rad] geodetic latitude, double from -pi/2 to pi/2
//		height_gd -- [m] geodetic altitude, positive double
// outputs:
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi (address of the required variable)
//		rad_gc -- [m] geocentric radial distance, positive double, should be greater than SMI_WGS84 (address of the required variable)
void GD2SEU(double lat_gd, double height_gd, double* colat_gc, double* rad_gc)
{
	// if prime vertical radius of curvature coincides with Z-axis of the Earth
	if ((DPI / 2 - lat_gd < VERYSMALL) || (DPI / 2 + lat_gd < VERYSMALL))
	{
		// phi_geod approximately equals to phi_geoc
		// theta_geoc = pi/2 - phi_geoc = pi/2 - phi_geod
		*colat_gc = DPI / 2 - lat_gd;
		// local Earth radius -> semi-minor axis, when latitudes tend to �pi/2 (or colatitude to 0 and pi)
		// radial distance in this case is semi-minor axis + satellite's altitude in geodetic system
		*rad_gc = SMI_WGS84 + height_gd;
	}
	else
	{
		// convert geodetic latitude into geocentric latitude
		// tan(phi_geoc) = b^2/a^2 * tan(phi_geod)
		double lat_gc = atan(B2_A2_WGS84 * tan(lat_gd));
		// compute geocentric polar angle (colatitude)
		// theta_geoc = pi/2 - phi_geoc
		*colat_gc = DPI / 2 - lat_gc;
		// prime vertical radius of curvature
		// N(phi_geod) = a / sqrt(1 - e^2 * sin^2(phi_geod))
		// source: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
		double PVR = SMA_WGS84 / sqrt(1 - E2_WGS84 * sin(lat_gd) * sin(lat_gd));
		// local Earth radius R
		// R = N(phi) * cos(phi_geod) / cos(phi_geoc)
		// source: sines theorem (triangle CENTER OF EARTH -- SATELLITE'S PROJECTION ON GEOID -- INTERSECTION OF PVR WITH Z-AXIS OF EARTH)
		double LER = PVR * cos(lat_gd) / cos(lat_gc);
		// geocentric radial distance r
		// r^2 = R^2 + h^2 + 2*R*h * cos(phi_geod - phi_geoc)
		// source: cosines thorem (triangle SATELLITE -- CENTER OF EARTH -- SATELLITE'S PROJECTION ON GEOID)
		*rad_gc = sqrt(LER * LER + height_gd * height_gd + 2 * LER * height_gd * cos(lat_gd - lat_gc));
	}
}

// convert geocentric parameters in SEU frame (SouthEastUp) to geodetic parameters in LLA (EastNorthUp)
// eastern longitude is identical in both frames
// source: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion
// inputs:
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		rad_gc -- [m] geocentric radial distance, positive double, should be greater than SMI_WGS84
// outputs:
//		lat_gd -- [rad] geodetic latitude, double from -pi/2 to pi/2 (address of the required variable)
//		height_gd -- [m] geodetic altitude, positive double (address of the required variable)
void SEU2GD(double colat_gc, double rad_gc, double* lat_gd, double* height_gd)
{
	// compute geocentric latitude
	// phi_geoc = pi/2 - theta_geoc
	double lat_gc = DPI / 2 - colat_gc;

	// if prime vertical radius of curvature coincides with Z-axis of the Earth
	if ((colat_gc < VERYSMALL) || (DPI - colat_gc < VERYSMALL))
	{
		// phi_geod approximately equals to phi_geoc
		*lat_gd = lat_gc;
		// local Earth radius -> semi-minor axis, when latitudes tend to �pi/2 (or colatitude to 0 and pi)
		// radial distance in this case is semi-minor axis + satellite's altitude in geodetic system
		*height_gd = rad_gc - SMI_WGS84;
	}
	else
	{
		// convert geodetic latitude into geocentric latitude
		// tan(phi_geoc) = b^2/a^2 * tan(phi_geod)
		*lat_gd = atan(tan(lat_gc) / B2_A2_WGS84);
		// prime vertical radius of curvature
		// N(phi_geod) = a / sqrt(1 - e^2 * sin^2(phi_geod))
		// source: https://en.wikipedia.org/wiki/Geographic_coordinate_conversion#From_geodetic_to_ECEF_coordinates
		double PVR = SMA_WGS84 / sqrt(1 - E2_WGS84 * sin(*lat_gd) * sin(*lat_gd));
		// local Earth radius R
		// R = N(phi) * cos(phi_geod) / cos(phi_geoc)
		// source: sines theorem (triangle CENTER OF EARTH -- SATELLITE'S PROJECTION ON GEOID -- INTERSECTION OF PVR WITH Z-AXIS OF EARTH)
		double LER = PVR * cos(*lat_gd) / cos(lat_gc);
		// geodetic altitude h
		// r^2 = R^2 + h^2 + 2*R*h * cos(phi_geod - phi_geoc)
		// source: cosines thorem (triangle SATELLITE -- CENTER OF EARTH -- SATELLITE'S PROJECTION ON GEOID)
		// real solution of the quadratic equation on h:
		// h = -R * cos(phi_geod - phi_geoc) + sqrt(r^2 - R^2 * sin^2(phi_geod - phi_geoc))
		*height_gd = sqrt(rad_gc * rad_gc - LER * LER * sin(*lat_gd - lat_gc) * sin(*lat_gd - lat_gc)) - LER * cos(*lat_gd - lat_gc);
	}
}

// block for ECI-LVLH conversions

// compute DCM for reference frames conversion: ECI -> LVLH, given position and velocity in ECI frame
// inputs:
//		r_J2000 -- [any1] satellite's position in ECI, doubles
//		v_J2000 -- [any2] satellite's velocity in ECI, doubles
// outputs:
//		DCM_J2000_to_LVLH -- Directional Cosine Matrix for ECI -> LVLH conversion, -1 <= doubles <= 1
void J20002LVLH_DCM(double r_J2000[3], double v_J2000[3], double DCM_J2000_to_LVLH[3][3])
{
	double rv_J2000[3] = { 0,0,0 }, rvr_J2000[3] = { 0,0,0 };
	double r_J2000_norm[3] = { 0,0,0 }, rv_J2000_norm[3] = { 0,0,0 }, rvr_J2000_norm[3] = { 0,0,0 }, _ = 0;

	// rv_J2000 = [r_J2000 x v_J2000]
	// rvr_J2000 = [rv_J2000 x r_J2000]
	// [... x ...] -- cross-product
	iauPxp( r_J2000, v_J2000,  rv_J2000);
	iauPxp(rv_J2000, r_J2000, rvr_J2000);

	// given vectors are L2-normalized:
	// r_J2000_norm = r_J2000 / |r_J2000|
	// rv_J2000_norm = rv_J2000 / |rv_J2000|
	// rvr_J2000_norm = rvr_J2000 / |rvr_J2000|
	iauPn(  r_J2000, &_,   r_J2000_norm);
	iauPn( rv_J2000, &_,  rv_J2000_norm);
	iauPn(rvr_J2000, &_, rvr_J2000_norm);

	//						[ rvr_J2000_norm ]
	// DCM_J2000_to_LVLH =	[  rv_J2000_norm ]
	//						[   r_J2000_norm ]
	DCM_J2000_to_LVLH[0][0] = rvr_J2000_norm[0];
	DCM_J2000_to_LVLH[0][1] = rvr_J2000_norm[1];
	DCM_J2000_to_LVLH[0][2] = rvr_J2000_norm[2];

	DCM_J2000_to_LVLH[1][0] =  rv_J2000_norm[0];
	DCM_J2000_to_LVLH[1][1] =  rv_J2000_norm[1];
	DCM_J2000_to_LVLH[1][2] =  rv_J2000_norm[2];

	DCM_J2000_to_LVLH[2][0] =   r_J2000_norm[0];
	DCM_J2000_to_LVLH[2][1] =   r_J2000_norm[1];
	DCM_J2000_to_LVLH[2][2] =   r_J2000_norm[2];
}

// compute quaternion for reference frames conversion: ECI -> LVLH, given position and velocity in ECI frame
// inputs:
//		r_J2000 -- [any1] satellite's position in ECI, doubles
//		v_J2000 -- [any2] satellite's velocity in ECI, doubles
// outputs:
//		Q_J2000_to_LVLH -- quaternion for ECI -> LVLH conversion, -1 <= doubles <= 1
void J20002LVLH_Q(double r_J2000[3], double v_J2000[3], double Q_J2000_to_LVLH[4])
{
	// compute DCM_J2000_to_LVLH for the given RV
	double DCM_J2000_to_LVLH[3][3] = { {1,0,0}, {0,1,0}, {0,0,1} };
	J20002LVLH_DCM(r_J2000, v_J2000, DCM_J2000_to_LVLH);

	// convert DCM inti quaternion
	DCM2QUAT(DCM_J2000_to_LVLH, Q_J2000_to_LVLH);
}

// compute position and velocity of satellite in LVLH frame, given position and velocity in ECI frame
// inputs:
//		r_J2000 -- [any1] satellite's position in ECI, doubles
//		v_J2000 -- [any2] satellite's velocity in ECI, doubles
// outputs:
//		r_LVLH -- [any1] satellite's position in LVLH, doubles
//		v_LVLH -- [any2] satellite's velocity in LVLH, doubles
void J20002LVLH_RV(double r_J2000[3], double v_J2000[3], double r_LVLH[3], double v_LVLH[3])
{
	// compute DCM_J2000_to_LVLH for the given RV
	double DCM_J2000_to_LVLH[3][3] = { {1,0,0}, {0,1,0}, {0,0,1} };
	J20002LVLH_DCM(r_J2000, v_J2000, DCM_J2000_to_LVLH);

	// r_LVLH = DCM_J2000_to_LVLH * r_J2000
	// v_LVLH = DCM_J2000_to_LVLH * v_J2000
	iauRxp(DCM_J2000_to_LVLH, r_J2000, r_LVLH);
	iauRxp(DCM_J2000_to_LVLH, v_J2000, v_LVLH);
}

// block for TEME-ECEF conversions
// source: https://github.com/skyfielders/python-skyfield/blob/master/skyfield/sgp4lib.py line 328 (TEME_to_ITRF)

// compute DCM for reference frames conversion: TEME -> ECEF for given Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
// outputs:
//		DCM_TEME_to_ITRS -- Directional Cosine Matrix for TEME -> ECEF conversion, -1 <= doubles <= 1
void TEME2ITRS_DCM(double JD_UTC, double DCM_TEME_to_ITRS[3][3])
{
	int flag = 0;
	double ut11 = 0, ut12 = 0, tt1 = 0, tt2 = 0, dut1 = 0, xp = 0, yp = 0;

	// dut1 = UT1-UTC [s] for the current day
	// xp, yp -- coordinates of the pole in ITRS in radians
	get_DUT1_and_TIP_coords(JD_UTC, &dut1, &xp, &yp);

	// dt = TT-UT1 [s]
	double dt = TTMTAI + DAT - dut1;
	// ut11 + ut12 = ut1 -- current UT1 time [JD]
	// flag -- error status (should be 0)
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.354
	flag = iauUtcut1(JD_UTC, 0, dut1, &ut11, &ut12);
	// tt1 + tt2 = tt -- current TT time [JD]
	// flag -- error status (should be 0)
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.351
	flag = iauUt1tt(ut11, ut12, dt, &tt1, &tt2);

	// calculate Greenwich Mean Sidereal Time angle in radians, given current date in UT1 and TT
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.183
	double GMST = iauGmst06(ut11, ut12, tt1, tt2);

	// calculate DCM, which converts TEME frame to Terrestrial Intermediate Reference System (frame)
	// rotate identity matrix in Z-direction by the angle of GMST
	// DCM_TEME_to_TIRS = R3(GMST)
	double DCM_TEME_to_TIRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	iauRz(GMST, DCM_TEME_to_TIRS);

	// calculate DCM, which converts Terrestrial Intermediate Reference System to International Terrestrial Reference System (ECEF)
	double DCM_TIRS_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	// calculate Terrestrial Intermediate Origin locator in radians, given date in TT
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.318
	double tio_loc = iauSp00(tt1, tt2);
	// compute DCM_TIRS_to_ITRS, given pole's coordinates and TIO locator
	// source: https://www.iausofa.org/2021_0512_C/sofa/manual.pdf p.271
	iauPom00(xp, yp, tio_loc, DCM_TIRS_to_ITRS);

	// DCM_TEME_to_ITRS = DCM_TIRS_to_ITRS * DCM_TEME_to_TIRS
	iauRxr(DCM_TIRS_to_ITRS, DCM_TEME_to_TIRS, DCM_TEME_to_ITRS);
}

// compute position and velocity of satellite in ECEF frame, given position and velocity in TEME frame and current Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
//		r_TEME -- [any] satellite's position in TEME, doubles
//		v_TEME -- [any]/[s] satellite's velocity in TEME, doubles
// outputs:
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
void TEME2ITRS_RV_from_JD(double JD_UTC, double r_TEME[3], double v_TEME[3], double r_ITRS[3], double v_ITRS[3])
{
	double DCM_TEME_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} }, v_ITRS_rel[3] = { 0,0,0 }, v_ITRS_fr[3] = { 0,0,0 };

	// compute DCM_TEME_to_ITRS for the given date
	TEME2ITRS_DCM(JD_UTC, DCM_TEME_to_ITRS);

	// r_ITRS = DCM_TEME_to_ITRS * r_TEME
	iauRxp(DCM_TEME_to_ITRS, r_TEME, r_ITRS);
	// v_ITRS_rel = DCM_TEME_to_ITRS * v_TEME
	// v_ITRS_rel -- relative velocity
	iauRxp(DCM_TEME_to_ITRS, v_TEME, v_ITRS_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_ITRS_fr = [Omega_Earth x r_ITRS]
	// v_ITRS_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_ITRS, v_ITRS_fr);

	// v_ITRS = v_ITRS_rel - v_ITRS_fr
	// v_ITRS = A * v_TEME - [Omega_Earth x r_ITRS]
	iauPmp(v_ITRS_rel, v_ITRS_fr, v_ITRS);
}

// compute position and velocity of satellite in ECEF frame, given position and velocity in TEME frame and DCM for conversion TEME -> ECEF
// inputs:
//		DCM_TEME_to_ITRS -- Directional Cosine Matrix for TEME -> ECEF conversion, -1 <= doubles <= 1
//		r_TEME -- [any] satellite's position in TEME, doubles
//		v_TEME -- [any]/[s] satellite's velocity in TEME, doubles
// outputs:
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
void TEME2ITRS_RV_from_DCM(double DCM_TEME_to_ITRS[3][3], double r_TEME[3], double v_TEME[3], double r_ITRS[3], double v_ITRS[3])
{
	double v_ITRS_rel[3] = { 0,0,0 }, v_ITRS_fr[3] = { 0,0,0 };

	// r_ITRS = DCM_TEME_to_ITRS * r_TEME
	iauRxp(DCM_TEME_to_ITRS, r_TEME, r_ITRS);
	// v_ITRS_rel = DCM_TEME_to_ITRS * v_TEME
	// v_ITRS_rel -- relative velocity
	iauRxp(DCM_TEME_to_ITRS, v_TEME, v_ITRS_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_ITRS_fr = [Omega_Earth x r_ITRS]
	// v_ITRS_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_ITRS, v_ITRS_fr);

	// v_ITRS = v_ITRS_rel - v_ITRS_fr
	// v_ITRS = A * v_TEME - [Omega_Earth x r_ITRS]
	iauPmp(v_ITRS_rel, v_ITRS_fr, v_ITRS);
}

// compute DCM for reference frames conversion: ECEF -> TEME for given Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
// outputs:
//		DCM_ITRS_to_TEME -- Directional Cosine Matrix for ECEF -> TEME conversion, -1 <= doubles <= 1
void ITRS2TEME_DCM(double JD_UTC, double DCM_ITRS_to_TEME[3][3])
{
	// compute DCM_TEME_to_ITRS for the given date
	double DCM_TEME_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} };
	TEME2ITRS_DCM(JD_UTC, DCM_TEME_to_ITRS);

	// transpose DCM_TEME_to_ITRS to get DCM_ITRS_to_TEME
	// DCM_ITRS_to_TEME = (DCM_TEME_to_ITRS).T
	iauTr(DCM_TEME_to_ITRS, DCM_ITRS_to_TEME);
}

// compute position and velocity of satellite in TEME frame, given position and velocity in ECEF frame and current Julian date
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
// outputs:
//		r_TEME -- [any] satellite's position in TEME, doubles
//		v_TEME -- [any]/[s] satellite's velocity in TEME, doubles
void ITRS2TEME_RV_from_JD(double JD_UTC, double r_ITRS[3], double v_ITRS[3], double r_TEME[3], double v_TEME[3])
{
	double DCM_ITRS_to_TEME[3][3] = { {1,0,0},{0,1,0},{0,0,1} }, v_TEME_rel[3] = { 0,0,0 }, v_TEME_fr[3] = { 0,0,0 };

	// compute DCM_ITRS_to_TEME for the given date
	ITRS2TEME_DCM(JD_UTC, DCM_ITRS_to_TEME);

	// r_TEME = DCM_ITRS_to_TEME * r_ITRS
	iauRxp(DCM_ITRS_to_TEME, r_ITRS, r_TEME);
	// v_TEME_rel = DCM_ITRS_to_TEME * v_ITRS
	// v_TEME_rel -- relative velocity
	iauRxp(DCM_ITRS_to_TEME, v_ITRS, v_TEME_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_TEME_fr = [Omega_Earth x r_TEME]
	// v_TEME_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_TEME, v_TEME_fr);

	// v_TEME = v_TEME_rel + v_TEME_fr
	// v_TEME = A.T * v_ITRS + [Omega_Earth x r_TEME]
	iauPpp(v_TEME_rel, v_TEME_fr, v_TEME);
}

// compute position and velocity of satellite in TEME frame, given position and velocity in ECEF frame and DCM for conversion ECEF -> TEME
// inputs:
//		DCM_ITRS_to_TEME -- Directional Cosine Matrix for ECEF -> TEME conversion, -1 <= doubles <= 1
//		r_ITRS -- [any] satellite's position in ECEF, doubles
//		v_ITRS -- [any]/[s] satellite's velocity in ECEF, doubles
// outputs:
//		r_TEME -- [any] satellite's position in TEME, doubles
//		v_TEME -- [any]/[s] satellite's velocity in TEME, doubles
void ITRS2TEME_RV_from_DCM(double DCM_ITRS_to_TEME[3][3], double r_ITRS[3], double v_ITRS[3], double r_TEME[3], double v_TEME[3])
{
	double v_TEME_rel[3] = { 0,0,0 }, v_TEME_fr[3] = { 0,0,0 };

	// r_TEME = DCM_ITRS_to_TEME * r_ITRS
	iauRxp(DCM_ITRS_to_TEME, r_ITRS, r_TEME);
	// v_TEME_rel = DCM_ITRS_to_TEME * v_ITRS
	// v_TEME_rel -- relative velocity
	iauRxp(DCM_ITRS_to_TEME, v_ITRS, v_TEME_rel);

	// Omega_Earth -- angular velocity of the Earth [rad/s]
	double Omega_Earth[3] = { 0,0, OMEGA_EARTH };
	// v_TEME_fr = [Omega_Earth x r_TEME]
	// v_TEME_fr -- frame velocity
	// [... x ...] -- cross-product
	iauPxp(Omega_Earth, r_TEME, v_TEME_fr);

	// v_TEME = v_TEME_rel + v_TEME_fr
	// v_TEME = A.T * v_ITRS + [Omega_Earth x r_TEME]
	iauPpp(v_TEME_rel, v_TEME_fr, v_TEME);
}