#include "orb_det.h"
#include <map>

// number of days in a leap year, which passed, respective to the chosen month
std::map<int, int> DAYS_IN_MONTHS_PASSED_LEAP = {	{ 0, 0 },
													{ 1, 31 },
													{ 2, 60 },
													{ 3, 91 },
													{ 4, 121 },
													{ 5, 152 },
													{ 6, 182 },
													{ 7, 213 },
													{ 8, 244 },
													{ 9, 274 },
													{ 10, 305 },
													{ 11, 335 }		};

// number of days in a common year, which passed, respective to the chosen month
std::map<int, int> DAYS_IN_MONTHS_PASSED = {		{ 0, 0 },
													{ 1, 31 },
													{ 2, 59 },
													{ 3, 90 },
													{ 4, 120 },
													{ 5, 151 },
													{ 6, 181 },
													{ 7, 212 },
													{ 8, 243 },
													{ 9, 273 },
													{ 10, 304 },
													{ 11, 334 }		};

// convert Julian date to Gregorian year + fraction of the year
// inputs:
//		JD -- Julian date, positive double
// returns:
//		year -- Gregorian year with fraction of the year, positive double
double julian2days(double JD)
{
	// convert Julan date to Greorian year, month, day and fraction of the day
	int year = 0, month = 0, day = 0;
	double fd = 0;
	int flag = iauJd2cal(JD, 0, &year, &month, &day, &fd);

	// if leap year
	if (year % 4 == 0)
	{
		// fraction of the year = (number of days passed + fraction of the day) / 366 days
		return year + (DAYS_IN_MONTHS_PASSED_LEAP[month - 1] + day + fd) / 366;
	}
	// if common year
	else
	{
		// fraction of the year = (number of days passed + fraction of the day) / 365 days
		return year + (DAYS_IN_MONTHS_PASSED[month - 1] + day + fd) / 365;
	}
}

/* -----------------------------------------------------------------------------
*
*                           procedure days2mdhms
*
*  this procedure converts the day of the year, days, to the equivalent month
*    day, hour, minute and second.
*
*  algorithm     : set up array for the number of days per month
*                  find leap year - use 1900 because 2000 is a leap year
*                  loop through a temp value while the value is < the days
*                  perform int conversions to the correct day and month
*                  convert remainder into h m s using type conversions
*
*  author        : david vallado                  719-573-2600    1 mar 2001
*
*  inputs          description                    range / units
*    year        - year                           1900 .. 2100
*    days        - julian day of the year         1.0  .. 366.0
*
*  outputs       :
*    mon         - month                          1 .. 12
*    day         - day                            1 .. 28,29,30,31
*    hr          - hour                           0 .. 23
*    min         - minute                         0 .. 59
*    sec         - second                         0.0 .. 59.999
*
*  locals        :
*    dayofyr     - day of year
*    temp        - temporary extended values
*    inttemp     - temporary int value
*    i           - index
*    lmonth[13]  - int array containing the number of days per month
*
*  coupling      :
*    none.
* --------------------------------------------------------------------------- */
void days2mdhms(int year, double days, int& mon, int& day, int& hr, int& minute, double& sec)
{
	int i, inttemp, dayofyr;
	double    temp;
	int lmonth[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

	dayofyr = (int)floor(days);
	/* ----------------- find month and day of month ---------------- */
	if ((year % 4) == 0)
		lmonth[2] = 29;

	i = 1;
	inttemp = 0;
	while ((dayofyr > inttemp + lmonth[i]) && (i < 12))
	{
		inttemp = inttemp + lmonth[i];
		i++;
	}
	mon = i;
	day = dayofyr - inttemp;

	/* ----------------- find hours minutes and seconds ------------- */
	temp = (days - dayofyr) * 24.0;
	hr = (int)floor(temp);
	temp = (temp - hr) * 60.0;
	minute = (int)floor(temp);
	sec = (temp - minute) * 60.0;
}