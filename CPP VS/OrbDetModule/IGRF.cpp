#include "IGRF.h"
#include <array>

// dictionary for factorials up to 20!
std::array<double, 21> FACTORIAL = { 1, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800, 39916800, 479001600,
	6227020800, 87178291200, 1307674368000, 20922789888000, 355687428096000, 6402373705728000,
	121645100408832000, 2432902008176640000 };

// excess for factorials from 21! to 26! (truncation by 20!)
// {21, 21 * 22, 21 * 22 * 23, ..., 21 * 22 * 23 * 24 * 25 * 26}
std::array<int, 6> FACTORIAL_EXCESS = { 21, 462, 10626, 255024, 6375600, 165765600 };

// SCHMIDT_NORMS[n][m] = (-1)^m * sqrt(2 * (n - m)! / (n + m)!)
// source: https://ru.wikipedia.org/wiki/����������_��������#����������_��_�������_������
// source: https://wikimedia.org/api/rest_v1/media/math/render/svg/d2f115d5c7df9d36ce576dadeabaec354f6231f4
std::array<std::array<double, 14>, 14> SCHMIDT_NORMS = { { {1.41421356237309514547e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-5.77350269189625731059e-01,2.88675134594812865529e-01,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
	                                                       {1.41421356237309514547e+00,-4.08248290463863017230e-01,1.29099444873580548876e-01,-5.27046276694729878831e-02,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-3.16227766016837941176e-01,7.45355992499929925765e-02,-1.99204768222398936883e-02,7.04295212273763852201e-03,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-2.58198889747161097752e-01,4.87950036474266643505e-02,-9.96023841111994684416e-03,2.34765070757921284067e-03,-7.42392338645623292397e-04,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-2.18217890235992362236e-01,3.45032779671177111669e-02,-5.75054632785295186115e-03,1.04990131391452003184e-03,-2.23839712229272313733e-04,6.46169590554493612116e-05,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.88982236504613598793e-01,2.57172249936819842520e-02,-3.63696483726653986210e-03,5.48293079133140839540e-04,-9.13821798555234732567e-05,1.79215199337676774505e-05,-4.78972767445701946116e-06,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.66666666666666657415e-01,1.99204768222398936883e-02,-2.45204119306874877837e-03,3.16557156832327640534e-04,-4.38985792528481940603e-05,6.77369783729086151361e-06,-1.23670236773605443889e-06,3.09175591934013609722e-07,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.49071198499985985153e-01,1.58910431540932040040e-02,-1.73385495536766448717e-03,1.96320414650060968108e-04,-2.34647776186143886944e-05,3.02928976464513510734e-06,-4.37240315267811748074e-07,7.49860954356942210760e-08,-1.76743921924269986274e-08,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.34839972492648424440e-01,1.29749824026920493325e-02,-1.27230170115095694329e-03,1.28521880085574551587e-04,-1.35473956745817230272e-05,1.51464488232256755367e-06,-1.83677671621093482726e-07,2.49953651452314059224e-08,-4.05478365554176709016e-09,9.06677188784648454706e-10,0.00000000000000000000e+00,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.23091490979332737754e-01,1.07958379271882627670e-02,-9.61769683968529396668e-04,8.77971585056963881206e-05,-8.29605168657828076593e-06,8.21431519387298054854e-07,-8.65864847705540519395e-08,9.93215097345443843493e-09,-1.28223517707356087275e-09,1.97853183261686551016e-10,-4.21824404046294487462e-11,0.00000000000000000000e+00,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.13227703414459576758e-01,9.12414835752264896662e-03,-7.44983593779456978491e-04,6.20819661482880860584e-05,-5.32348467864826782180e-06,4.74253708839099801156e-07,-4.44179294788696680671e-08,4.44179294788696664127e-09,-4.84639342976501538839e-10,5.96549793142217913223e-11,-8.79564684446878439288e-12,1.79540389388912611397e-12,0.00000000000000000000e+00},
														   {1.41421356237309514547e+00,-1.04828483672191830056e-01,7.81345384897492306608e-03,-5.88961243950182904323e-04,4.51712653897785447769e-05,-3.54898978576551145768e-06,2.87861083312720907265e-07,-2.43287019332506041953e-08,2.16737303093258553585e-09,-2.06650910193405038126e-10,2.15448467266695149833e-11,-2.53908453667546765319e-12,3.59080778777825253087e-13,-7.04215345363163314247e-14} } };

// IGRF_2020_G[n][m] -- restructured penultimate column "IGRF 2020.0" from 
// source: https://www.ngdc.noaa.gov/IAGA/vmod/coeffs/igrf13coeffs.txt
// from "g/h" rows "g"-s are chosen
// respective "n" and "m" values are inserted into the table
// nanoTesla
std::array<std::array<double, 14>, 14> IGRF_2020_G = { { {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {-29404.8, -1450.9, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {-2499.6, 2982.0, 1677.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {1363.2, -2381.2, 1236.2, 525.7, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {903.0, 809.5, 86.3, -309.4, 48.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {-234.3, 363.2, 187.8, -140.7, -151.2, 13.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {66.0, 65.5, 72.9, -121.5, -36.2, 13.5, -64.7, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {80.6, -76.7, -8.2, 56.5, 15.8, 6.4, -7.2, 9.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {23.7, 9.7, -17.6, -0.5, -21.1, 15.3, 13.7, -16.5, -0.3, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {5.0, 8.4, 2.9, -1.5, -1.1, -13.2, 1.1, 8.8, -9.3, -11.9, 0.0, 0.0, 0.0, 0.0},
														 {-1.9, -6.2, -0.1, 1.7, -0.9, 0.7, -0.9, 1.9, 1.4, -2.4, -3.8, 0.0, 0.0, 0.0},
														 {3.0, -1.4, -2.5, 2.3, -0.9, 0.3, -0.7, -0.1, 1.4, -0.6, 0.2, 3.1, 0.0, 0.0},
														 {-2.0, -0.1, 0.5, 1.3, -1.2, 0.7, 0.3, 0.5, -0.3, -0.5, 0.1, -1.1, -0.3, 0.0},
														 {0.1, -0.9, 0.5, 0.7, -0.3, 0.8, 0.0, 0.8, 0.0, 0.4, 0.1, 0.5, -0.5, -0.4} } };

// IGRF_2020_H[n][m] -- restructured penultimate column "IGRF 2020.0" from 
// source: https://www.ngdc.noaa.gov/IAGA/vmod/coeffs/igrf13coeffs.txt
// from "g/h" rows "h"-s are chosen
// respective "n" and "m" values are inserted into the table
// nanoTesla
std::array<std::array<double, 14>, 14> IGRF_2020_H = { { {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, 4652.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, -2991.6, -734.6, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, -82.1, 241.9, -543.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, 281.9, -158.4, 199.7, -349.7, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, 47.7, 208.3, -121.2, 32.3, 98.9, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, -19.1, 25.1, 52.8, -64.5, 8.9, 68.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, -51.5, -16.9, 2.2, 23.5, -2.2, -27.2, -1.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, 8.4, -15.3, 12.8, -11.7, 14.9, 3.6, -6.9, 2.8, 0.0, 0.0, 0.0, 0.0, 0.0},
														 {0.0, -23.4, 11.0, 9.8, -5.1, -6.3, 7.8, 0.4, -1.4, 9.6, 0.0, 0.0, 0.0, 0.0},
														 {0.0, 3.4, -0.2, 3.6, 4.8, -8.6, -0.1, -4.3, -3.4, -0.1, -8.8, 0.0, 0.0, 0.0},
														 {0.0, 0.0, 2.5, -0.6, -0.4, 0.6, -0.2, -1.7, -1.6, -3.0, -2.0, -2.6, 0.0, 0.0},
														 {0.0, -1.2, 0.5, 1.4, -1.8, 0.1, 0.8, -0.2, 0.6, 0.2, -0.9, 0.0, 0.5, 0.0},
														 {0.0, -0.9, 0.6, 1.4, -0.4, -1.3, -0.1, 0.3, -0.1, 0.5, 0.5, -0.4, -0.4, -0.6} } };

// SV_G[n][m] -- restructured last column "SV 2020-25" from 
// source: https://www.ngdc.noaa.gov/IAGA/vmod/coeffs/igrf13coeffs.txt
// from "g/h" rows "g"-s are chosen
// respective "n" and "m" values are inserted into the table
// nanoTesla / year
std::array<std::array<double, 14>, 14> SV_G = { { {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {5.7, 7.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {-11.0, -7.0, -2.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {2.2, -5.9, 3.1, -12.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {-1.2, -1.6, -5.9, 5.2, -5.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {-0.3, 0.5, -0.6, 0.2, 1.3, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {-0.5, -0.3, 0.4, 1.3, -1.4, 0.0, 0.9, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {-0.1, -0.2, 0.0, 0.7, 0.1, -0.5, -0.8, 0.8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.1, -0.1, 0.4, -0.1, 0.4, 0.3, -0.1, 0.4, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0} } };

// SV_H[n][m] -- restructured last column "SV 2020-25" from 
// source: https://www.ngdc.noaa.gov/IAGA/vmod/coeffs/igrf13coeffs.txt
// from "g/h" rows "h"-s are chosen
// respective "n" and "m" values are inserted into the table
// nanoTesla / year
std::array<std::array<double, 14>, 14> SV_H = { { {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, -25.9, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, -30.2, -22.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 6.0, -1.1, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, -0.1, 6.5, 3.6, -5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 2.5, -0.6, 3.0, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, -1.6, -1.3, 0.8, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.6, 0.6, -0.8, -0.2, -1.1, 0.1, 0.3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, -0.2, 0.6, -0.2, 0.5, -0.3, -0.4, 0.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
												  {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0} } };

// compute �(n + 0.5), where n is integer
// source: https://en.wikipedia.org/wiki/Gamma_function#General
// source: https://wikimedia.org/api/rest_v1/media/math/render/svg/1e88441c75db099825561c7f17455c5379f3311e
// inputs:
//		n -- positive integer
// returns:
//		�(n + 0.5) -- Gamma-function in n+0.5, positive double
double Gamma_n_plus_half(int n)
{
	// if 2n > 20, there is not enough space to store respective factorials
	if (2 * n > 20)
	{
		// firstly calculate �(n+1/2) with truncation on 20!
		// �(n+1/2) = 20! / n! * sqrt(pi) / 4^n
		// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
		double curr = FACTORIAL[20] / FACTORIAL[n] * sqrt(DPI) / pow(4, n);
		// and then multiply by the rest
		// �(n+1/2) *= (2n)! / 20!
		// FACTORIAL_EXCESS[k] -- direct dictionary for factorials excess after 20
		return curr * FACTORIAL_EXCESS[2 * n - 21];
	}
	// if 2n <= 20
	else
	{
		// �(n+1/2) = (2n)! / n! * sqrt(pi) / 4^n
		return FACTORIAL[2 * n] / FACTORIAL[n] * sqrt(DPI) / pow(4, n);
	}
}

// compute �(0.5 - n), where n is integer
// source: https://en.wikipedia.org/wiki/Gamma_function#General
// source: https://wikimedia.org/api/rest_v1/media/math/render/svg/1e88441c75db099825561c7f17455c5379f3311e
// inputs:
//		n -- positive integer
// returns:
//		�(0.5 - n) -- Gamma-function in 0.5-n, positive double
double Gamma_half_minus_n(int n)
{
	// if 2n > 20, there is not enough space to store respective factorials
	if (2 * n > 20)
	{
		// firstly calculate �(1/2-n) with truncation on 20!
		// �(1/2-n) = n! / 20! * sqrt(pi) * (-4)^n
		// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
		double curr = FACTORIAL[n] / FACTORIAL[20] * sqrt(DPI) * pow(-4, n);
		// and then divide by the rest
		// �(1/2-n) /= (2n)! / 20!
		// FACTORIAL_EXCESS[k] -- direct dictionary for factorials excess after 20
		return curr / FACTORIAL_EXCESS[2 * n - 21];
	}
	else
	{
		// �(1/2-n) = n! / (2n)! * sqrt(pi) * (-4)^n
		return FACTORIAL[n] / FACTORIAL[2 * n] * sqrt(DPI) * pow(-4, n);
	}
}

// compute binomial coefficient C^k_n only for 2 cases:
// 1) n is integer
// 2) n = m - 0.5, where m is integer
// other cases require explicit Gamma-function calculation
// source: https://en.wikipedia.org/wiki/Binomial_coefficient#Two_real_or_complex_valued_arguments
// source: https://wikimedia.org/api/rest_v1/media/math/render/svg/a3db03bed6272cfc9df6e7be2610aed299679e7e
// inputs:
//		n -- positive double
//		k -- positive integer
// returns:
//		C^k_n -- binomial coefficient "n choose k", non-negative double
double binom(double n, int k)
{
	// if n = m - 0.5, where m is integer
	// this case allows k to be bigger than n
	if (abs(int(n - 0.5) - n + 0.5) < SMALL)
	//if (int(n - 0.5) == n - 0.5)
	{
		double m = n + 0.5;
		if (m >= k)
		{
			// C^k_n = �(n + 1) / (�(k + 1) * �(n - k + 1))
			// since k is integer: �(k + 1) = k!
			// C^k_n = �((n + 0.5) + 0.5) / (k! * �((n + 0.5 - k) + 0.5))
			// C^k_n = �(m + 0.5) / (k! * �((m - k) + 0.5))
			// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
			return Gamma_n_plus_half(int(m)) / FACTORIAL[k] / Gamma_n_plus_half(int(m - k));
		}
		else
		{
			// C^k_n = �(m + 0.5) / (k! * �((m - k) + 0.5))
			// m - k < 0, so the value (m - k) + 0.5 can be rewritten as
			// (m - k) + 0.5 = 0.5 - (k - m)
			// C^k_n = �(m + 0.5) / (k! * �(0.5 - (k - m)))
			// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
			return Gamma_n_plus_half(int(m)) / FACTORIAL[k] / Gamma_half_minus_n(int(k - m));
		}
	}
	// if n is integer
	else
	{
		if ((k < 0) || (k > n))
		{
			// in impossible cases coefficient is 0
			return 0;
		}
		else
		{
			// C^k_n = n! / (k! * (n-k)!)
			// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
			return FACTORIAL[int(n)] / FACTORIAL[k] / FACTORIAL[int(n - k)];
		}
	}
	return 0;
}

// compute associated Legendre polynomials P{n,m}(cos(theta))
// source: https://en.wikipedia.org/wiki/Associated_Legendre_polynomials#Closed_Form
// source: https://wikimedia.org/api/rest_v1/media/math/render/svg/6420abf1d085ef2b473534c7bf162e3cb026812d
// inputs:
//		n -- degree of associated Legendre polynomial, non-negative integer
//		m -- order of associated Legendre polynomial, non-negative integer
//		theta -- [rad] polar angle (colatitude), double from 0 to pi
// returns:
//		P{n,m}(cos(theta)) -- associated Legendre polynomial of degree n and order m in polar angle theta, double
double assoc_legendre(double theta, int n, int m)
{
	// accumulation value
	double sum = 0;

	// cosine of theta, frequently used further
	double cos_theta = cos(theta);

	// for all orders k of associated Legendre polynomial (m, ..., n)
	for (int k = m; k < n + 1; k++)
	{
		// s += k!/(k-m)! * cos(theta)^(k-m) * C^k_n * C^((n + k - 1) / 2)_n
		// C^k_n -- generalized binomial coefficient
		// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
		sum += FACTORIAL[k] / FACTORIAL[k - m] * pow(cos_theta, k - m) * binom(n, k) * binom((n + k - 1) / (double)2, n);
	}

	// P{n,m}(cos(theta)) = (-1)^m * 2^n * sin(theta)^m * s
	return pow(-1, m) * pow(2, n) * pow(sin(theta), m) * sum;
}

// compute derivative of associated Legendre polynomial by theta d(P{n,m}(cos(theta)))/d(theta)
// source: manual differentiatin of P{n,m}(cos(theta)) by theta
// inputs:
//		n -- degree of associated Legendre polynomial, non-negative integer
//		m -- order of associated Legendre polynomial, non-negative integer
//		theta -- [rad] polar angle (colatitude), double from 0 to pi
// returns:
//		d(P{n,m}(cos(theta)))/d(theta) -- derivative by theta of the associated Legendre polynomial of degree n and order m in polar angle theta, double
double assoc_legendre_deriv(double theta, int n, int m)
{	
	// main accumulation value
	double SUM = 0;
	// supplementary accumulation value (for 2 parts)
	double sum = 0;

	// cosine and sine of theta, frequently used further
	double cos_theta = cos(theta);
	double sin_theta = sin(theta);

	// the derivative of P(cos(theta)) consists of 2 parts
	// the first one nullifies when m = 0
	if (m != 0)
	{
		// for all orders k of associated Legendre polynomial (m, ..., n)
		for (int k = m; k < n + 1; k++)
		{
			// s += k!/(k-m)! * cos(theta)^(k-m) * C^k_n * C^((n + k - 1) / 2)_n
			// C^k_n -- generalized binomial coefficient
			// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
			sum += FACTORIAL[k] / FACTORIAL[k - m] * pow(cos_theta, k - m) * binom(n, k) * binom((n + k - 1) / (double)2, n);
		}

		// S += m * sin(theta)^(m-1) * cos(theta) * s
		SUM += m * pow(sin_theta, m - 1) * cos_theta * sum;

		// nullifying s for the second part
		sum = 0;
	}

	// the second part
	// for all orders k of associated Legendre polynomial (m+1, ..., n)
	for (int k = m + 1; k < n + 1; k++)
	{
		// s += k!/(k-m-1)! * cos(theta)^(k-m-1) * (-sin(theta)) * C^k_n * C^((n + k - 1) / 2)_n
		// C^k_n -- generalized binomial coefficient
		// FACTORIAL[k] -- direct dictionary for factorials, computationally effective
		sum += FACTORIAL[k] / FACTORIAL[k - m - 1] * pow(cos_theta, k - m - 1) * (-sin_theta) * binom(n, k) * binom((n + k - 1) / (double)2, n);
	}

	// S += sin(theta)^m * s
	SUM += pow(sin_theta, m) * sum;

	// d(P{n,m}(cos(theta)))/d(theta) = (-1)^m * 2^n * S
	return pow(-1, m) * pow(2, n) * SUM;
}

// compute Schmidt normalized associated Legendre polynomials SP{n,m}(cos(theta))
// source: https://ru.wikipedia.org/wiki/����������_��������#����������_��_�������_������
// source: https://wikimedia.org/api/rest_v1/media/math/render/svg/d2f115d5c7df9d36ce576dadeabaec354f6231f4
// inputs:
//		n -- degree of associated Legendre polynomial, non-negative integer
//		m -- order of associated Legendre polynomial, non-negative integer
//		theta -- [rad] polar angle (colatitude), double from 0 to pi
// returns:
//		SP{n,m}(cos(theta)) -- Schmidt normalized associated Legendre polynomial of degree n and order m in polar angle theta, double
double assoc_legendre_norm(double theta, int n, int m)
{
	// if order m is 0, normalization is not required
	if (m == 0)
	{
		// associated Legendre polynomial P{n,m}(cos(theta))
		return assoc_legendre(theta, n, m);
	}
	// if order m is not 0, normalization is required
	else
	{
		// SCHMIDT_NORMS[n][m] -- direct dictionary for norms, since straightforward computation requires to store value up to 26!
		return SCHMIDT_NORMS[n][m] * assoc_legendre(theta, n, m);
	}
}

// compute derivative of Schmidt normalized associated Legendre polynomial by theta d(SP{n,m}(cos(theta)))/d(theta)
// source: https://ru.wikipedia.org/wiki/����������_��������#����������_��_�������_������
// source: https://wikimedia.org/api/rest_v1/media/math/render/svg/d2f115d5c7df9d36ce576dadeabaec354f6231f4
// inputs:
//		n -- degree of associated Legendre polynomial, non-negative integer
//		m -- order of associated Legendre polynomial, non-negative integer
//		theta -- [rad] polar angle (colatitude), double from 0 to pi
// returns:
//		d(SP{n,m}(cos(theta)))/d(theta) -- derivative by theta of Schmidt normalized associated Legendre polynomial of degree n and order m in polar angle theta, double
double assoc_legendre_deriv_norm(double theta, int n, int m)
{
	// if order m is 0, normalization is not required
	if (m == 0)
	{
		// derivative of associated Legendre polynomial by theta d(P{n,m}(cos(theta)))/d(theta)
		return assoc_legendre_deriv(theta, n, m);
	}
	// if order m is not 0, normalization is required
	else
	{
		// SCHMIDT_NORMS[n][m] -- direct dictionary for norms, since straightforward computation requires to store value up to 26!
		return SCHMIDT_NORMS[n][m] * assoc_legendre_deriv(theta, n, m);
	}
}

// compute derivative of the scalar potential V by theta dV/d(theta)
// V -- scalar potential of the magnetic field from:
// source: https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html
// source: manual differentiation of V by theta
// inputs:
//		year -- amount of Gregorian years, positive double (>= 2020)
//		rad_gc -- [m] geocentric radial distance, positive double
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// returns:
//		dV/d(theta) -- derivative of the scalar potential V by theta, double
double colat_gc_deriv(double year, double rad_gc, double colat_gc, double elong)
{
	// amount of years after 2020 for secular variation of the field
	double delta_year = year - 2020;

	// initializaion of dV/d(theta)
	double sum = 0;

	// for all degrees n of spherical harmonic function (1, ..., N), N -- degree of truncation
	for (int n = 1; n < N_IGRF + 1; n++)
	{
		// for all orders m of spherical harmonic function (0, ..., n)
		for (int m = 0; m < n + 1; m++)
		{
			// 1/a dV/d(theta) += (a / r)^(n+1) * [ g{n,m}(t)*cos(m*lambda) + h{n,m}(t)*sin(m*lambda) ] * d(SP{n,m}(cos(theta)))/d(theta)
			// a -- mean equatorial radius of the Earth [m]
			// r -- geocentric radial distance [m]
			// theta -- geocentric polar angle (colatitude) [rad]
			// lambda -- geocentric eastern longitude [rad] (in sources is given as phi)
			// SP{n,m}(cos(theta)) -- Schmidt normalized associated Legendre polynomials
			// d(SP{n,m}(cos(theta)))/d(theta) -- derivatives of Schmidt normalized associated Legendre polynomials by theta
			// g{n,m}(t) -- spherical harmonic coefficients for cosines at given time t [nT]
			// h{n,m}(t) -- spherical harmonic coefficients for sines at given time t [nT]
			// IGRF_2020_G[n][m], IGRF_2020_H[n][m] -- coefficients at 0h, 1 Jan 2020 [nT]
			// SV_G[n][m], SV_H[n][m] -- secular variation of previous coefficients [nT/year] (viable till 0h, 1 Jan 2025)
			// source: https://www.ngdc.noaa.gov/IAGA/vmod/coeffs/igrf13coeffs.txt
			sum += pow(MEAN_EQUAT_RAD / rad_gc, n + 1) * ((IGRF_2020_G[n][m] + delta_year * SV_G[n][m]) * cos(m * elong) + (IGRF_2020_H[n][m] + delta_year * SV_H[n][m]) * sin(m * elong)) * assoc_legendre_deriv_norm(colat_gc, n, m);
		}
	}

	// dV/d(theta) = 1/a dV/d(theta) * a
	return sum * MEAN_EQUAT_RAD;
}

// compute derivative of the scalar potential V by r dV/dr
// V -- scalar potential of the magnetic field from:
// source: https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html
// source: manual differentiation of V by r
// inputs:
//		year -- amount of Gregorian years, positive double (>= 2020)
//		rad_gc -- [m] geocentric radial distance, positive double
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// returns:
//		dV/dr -- derivative of the scalar potential V by r, double
double rad_gc_deriv(double year, double rad_gc, double colat_gc, double elong)
{
	// amount of years after 2020 for secular variation of the field
	double delta_year = year - 2020;

	// initializaion of dV/dr
	double sum = 0;

	// for all degrees n of spherical harmonic function (1, ..., N), N -- degree of truncation
	for (int n = 1; n < N_IGRF + 1; n++)
	{
		// for all orders m of spherical harmonic function (0, ..., n)
		for (int m = 0; m < n + 1; m++)
		{
			// -dV/dr += (n+1) * (a / r)^(n+2) * [ g{n,m}(t)*cos(m*lambda) + h{n,m}(t)*sin(m*lambda) ] * SP{n,m}(cos(theta))
			// source: differentiation of V by r
			// a -- mean equatorial radius of the Earth [m]
			// r -- geocentric radial distance [m]
			// theta -- geocentric polar angle (colatitude) [rad]
			// lambda -- geocentric eastern longitude [rad] (in sources is given as phi)
			// SP{n,m}(cos(theta)) -- Schmidt normalized associated Legendre polynomials
			// g{n,m}(t) -- spherical harmonic coefficients for cosines at given time t [nT]
			// h{n,m}(t) -- spherical harmonic coefficients for sines at given time t [nT]
			// IGRF_2020_G[n][m], IGRF_2020_H[n][m] -- coefficients at 0h, 1 Jan 2020 [nT]
			// SV_G[n][m], SV_H[n][m] -- secular variation of previous coefficients [nT/year] (viable till 0h, 1 Jan 2025)
			// source: https://www.ngdc.noaa.gov/IAGA/vmod/coeffs/igrf13coeffs.txt
			sum += (n + 1) * pow(MEAN_EQUAT_RAD / rad_gc, n + 2) * ((IGRF_2020_G[n][m] + delta_year * SV_G[n][m]) * cos(m * elong) + (IGRF_2020_H[n][m] + delta_year * SV_H[n][m]) * sin(m * elong)) * assoc_legendre_norm(colat_gc, n, m);
		}
	}

	// dV/dr = -dV/dr * (-1)
	return sum * (-1);
}

// compute derivative of the scalar potential V by lambda dV/d(lambda)
// V -- scalar potential of the magnetic field from:
// source: https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html
// source: manual differentiation of V by lambda
// inputs:
//		year -- amount of Gregorian years, positive double (>= 2020)
//		rad_gc -- [m] geocentric radial distance, positive double
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// returns:
//		dV/d(lambda) -- derivative of the scalar potential V by lambda, double
double elong_deriv(double year, double rad_gc, double colat_gc, double elong)
{
	// amount of years after 2020 for secular variation of the field
	double delta_year = year - 2020;

	// initializaion of dV/d(lambda)
	double sum = 0;

	// for all degrees n of spherical harmonic function (1, ..., N), N -- degree of truncation
	for (int n = 1; n < N_IGRF + 1; n++)
	{
		// for all orders m of spherical harmonic function (0, ..., n)
		for (int m = 0; m < n + 1; m++)
		{
			// 1/a dV/d(lambda) += (a / r)^(n+1) * m * [ h{n,m}(t)*cos(m*lambda) - g{n,m}(t)*sin(m*lambda) ] * SP{n,m}(cos(theta))
			// source: differentiation of V by lambda
			// a -- mean equatorial radius of the Earth [m]
			// r -- geocentric radial distance [m]
			// theta -- geocentric polar angle (colatitude) [rad]
			// lambda -- geocentric eastern longitude [rad] (in sources is given as phi)
			// SP{n,m}(cos(theta)) -- Schmidt normalized associated Legendre polynomials
			// g{n,m}(t) -- spherical harmonic coefficients for cosines at given time t [nT] (changed for sines after differentiation)
			// h{n,m}(t) -- spherical harmonic coefficients for sines at given time t [nT] (changed for cosines after differentiation)
			// IGRF_2020_G[n][m], IGRF_2020_H[n][m] -- coefficients at 0h, 1 Jan 2020 [nT]
			// SV_G[n][m], SV_H[n][m] -- secular variation of previous coefficients [nT/year] (viable till 0h, 1 Jan 2025)
			// source: https://www.ngdc.noaa.gov/IAGA/vmod/coeffs/igrf13coeffs.txt
			sum += pow(MEAN_EQUAT_RAD / rad_gc, n + 1) * m * ((IGRF_2020_H[n][m] + delta_year * SV_H[n][m]) * cos(m * elong) - (IGRF_2020_G[n][m] + delta_year * SV_G[n][m]) * sin(m * elong)) * assoc_legendre_norm(colat_gc, n, m);
		}
	}

	// dV/d(lambda) = 1/a dV/d(lambda) * a
	return sum * MEAN_EQUAT_RAD;
}

// compute IGRF-13 magnetic field in SouthEastUp topocentric (spherical) reference frame
// source: https://www.ngdc.noaa.gov/IAGA/vmod/igrf.html
// source: https://en.wikipedia.org/wiki/Spherical_coordinate_system#Integration_and_differentiation_in_spherical_coordinates
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
//		rad_gc -- [m] geocentric radial distance, positive double
//		colat_gc -- [rad] geocentric polar angle (colatitude), double from 0 to pi
//		elong -- [rad] geocentric eastern longitude, double from -pi to pi
// outputs:
//		mf_SEU -- [nT] magnetic field in SEU, doubles
void magn_field_SEU(double JD_UTC, double rad_gc, double colat_gc, double elong, double mf_SEU[3])
{
	// convert Julian day to years with fraction of year
	double year = julian2days(JD_UTC);

	// B = -grad(V)
	// V -- scalar potential of the magnetic field
	// since spherical coordinates are used, the -gradient will be following:
	// B = { -1/r dV/d(theta), -1/(r*sin(theta)) dV/d(lambda), -dV/dr }
	// r -- geocentric radial distance [m]
	// theta -- geocentric polar angle (colatitude) [rad]
	// lambda -- geocentric eastern longitude [rad] (in sources is given as phi)
	// B is in SouthEastUp topocentric reference frame

	// B_r = -dV/dr [nT]
	mf_SEU[2] = -rad_gc_deriv(year, rad_gc, colat_gc, elong);
	// B_theta = -1/r dV/d(theta) [nT]
	mf_SEU[0] = -colat_gc_deriv(year, rad_gc, colat_gc, elong) / rad_gc;

	// B_lambda = -1/(r*sin(theta)) dV/d(lambda) [nT]
	// if theta is close to 0 or pi, in denominator of B_lambda sin(theta) -> 0
	// in this case we interpolate this value from 2 close points
	if ((colat_gc < VERYSMALL) || (DPI - colat_gc < VERYSMALL))
	{
		double B_elong_west = -elong_deriv(year, rad_gc, VERYSMALL, elong - DPI) / (rad_gc * sin(VERYSMALL));
		double B_elong_east = -elong_deriv(year, rad_gc, VERYSMALL, elong) / (rad_gc * sin(VERYSMALL));
		mf_SEU[1] = (B_elong_west + B_elong_east) / 2;
	}
	else
	{
		mf_SEU[1] = -elong_deriv(year, rad_gc, colat_gc, elong) / (rad_gc * sin(colat_gc));
	}
}

// compute IGRF-13 magnetic field in ECEF reference frame
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
//		r_ITRS -- [m] satellite's position in ECEF, doubles
// outputs:
//		mf_ITRS -- [nT] magnetic field in ECEF, doubles
void magn_field_ITRS(double JD_UTC, double r_ITRS[3], double mf_ITRS[3])
{
	// convert ECEF position into LLA parameters (geodetic)
	double elong = 0, lat_gd = 0, height_gd = 0;
	ITRS2WGS84_R(r_ITRS, &elong, &lat_gd, &height_gd);

	// convert geodetic parameters to geocentric
	// elong is identical for both frames
	double colat_gc = 0, rad_gc = 0;
	GD2SEU(lat_gd, height_gd, &colat_gc, &rad_gc);

	// calculate IGRF magnetic field for the position in geocentric parameters (in nT)
	double mf_SEU[3] = { 0,0,0 };
	magn_field_SEU(JD_UTC, rad_gc, colat_gc, elong, mf_SEU);

	// convert magnetic field into ECEF
	SEU2ITRS_vec(mf_SEU, colat_gc, elong, mf_ITRS);
}

// compute IGRF-13 magnetic field in ECI reference frame
// inputs:
//		JD_UTC -- Julian date in UTC, positive double
//		r_J2000 -- [m] satellite's position in ECI, doubles
// outputs:
//		mf_J2000 -- [nT] magnetic field in ECI, doubles
void magn_field_J2000(double JD_UTC, double r_J2000[3], double mf_J2000[3])
{
	double DCM_J2000_to_ITRS[3][3] = { {1,0,0},{0,1,0},{0,0,1} }, DCM_ITRS_to_J2000[3][3] = { {1,0,0},{0,1,0},{0,0,1} },
		   r_ITRS[3] = { 0,0,0 }, mf_ITRS[3] = { 0,0,0 };

	// compute DCM_J2000_to_ITRS for the given date
	J20002ITRS_DCM(JD_UTC, DCM_J2000_to_ITRS);
	// r_ITRS = DCM_J2000_to_ITRS * r_J2000
	iauRxp(DCM_J2000_to_ITRS, r_J2000, r_ITRS);
	// compute mf_ITRS in the given position for the given date
	magn_field_ITRS(JD_UTC, r_ITRS, mf_ITRS);
	// transpose DCM_J2000_to_ITRS to get DCM_ITRS_to_J2000
	// DCM_ITRS_to_J2000 = (DCM_J2000_to_ITRS).T
	iauTr(DCM_J2000_to_ITRS, DCM_ITRS_to_J2000);
	// mf_J2000 = DCM_ITRS_to_J2000 * mf_ITRS
	iauRxp(DCM_ITRS_to_J2000, mf_ITRS, mf_J2000);
}