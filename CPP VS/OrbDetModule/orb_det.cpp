#include "orb_det.h"
#include <cstring>
#include <iostream>
#include <iomanip>

extern char TLE1_CURRENT[130];
extern char TLE2_CURRENT[130];
extern char TLE1_NORAD[130];
extern char TLE2_NORAD[130];

extern elsetrec SATREC_NORAD;
extern double JD_NORAD;

gravconsttype whichconst = wgs84;

// write uplinked NORAD TLE to global variables TLE_current and TLE_NORAD
// inputs:
//		tle1 -- first elements line, chars with fixed structure
//		tle2 -- second elements line, chars with fixed structure
void update_TLE_from_NORAD(char* tle1, char* tle2)
{
	// update global variables of current TLE
	std::strncpy(TLE1_CURRENT, tle1, sizeof(TLE1_CURRENT) - 1);
	TLE1_CURRENT[sizeof(TLE1_CURRENT) - 1] = 0;
	std::strncpy(TLE2_CURRENT, tle2, sizeof(TLE2_CURRENT) - 1);
	TLE2_CURRENT[sizeof(TLE2_CURRENT) - 1] = 0;

	// update global variables of NORAD TLE
	std::strncpy(TLE1_NORAD, tle1, sizeof(TLE1_NORAD) - 1);
	TLE1_NORAD[sizeof(TLE1_NORAD) - 1] = 0;
	std::strncpy(TLE2_NORAD, tle2, sizeof(TLE2_NORAD) - 1);
	TLE2_NORAD[sizeof(TLE2_NORAD) - 1] = 0;

	// extract parameters from NORAD TLE and put them into sgp4 structure (SATREC_NORAD)
	SGP4Funcs::sgp4_init_TLE(TLE1_NORAD, TLE2_NORAD, SATREC_NORAD);
	// calculate Julian day, corresponding to NORAD TLE
	JD_NORAD = SATREC_NORAD.jdsatepoch + SATREC_NORAD.jdsatepochF;
	// update structure of satellite parameters (SATREC_NORAD), based on NORAD TLE
	SGP4Funcs::sgp4init(whichconst, 'i', SATREC_NORAD.satnum, JD_NORAD - JD_SGP4_REF, SATREC_NORAD.bstar,
		SATREC_NORAD.ndot, SATREC_NORAD.nddot, SATREC_NORAD.ecco, SATREC_NORAD.argpo, SATREC_NORAD.inclo, SATREC_NORAD.mo, SATREC_NORAD.no_kozai,
		SATREC_NORAD.nodeo, SATREC_NORAD);
}

// update TLE_current, based on the GPS readings, verify the correctness of GPS data
// inputs:
//		gps_current -- new GPS data, structure gps_data_time_s
// returns:
//		int status:	0 -- everything is good, TLE_current is updated
//					1 -- invalid time (Julian date from GPS is not greater then NORAD's date + 1 day), TLE_current is not updated
//					2 -- invalid propagation results (R from GPS is different from NORAD's propagation by more than 5 km),
//						 TLE_current is not updated
int update_TLE_from_GPS(gps_data_time_s* gps_current)
{
	double R_ITRS[3] = { 0,0,0 }, V_ITRS[3] = { 0,0,0 }, V_WGS84[3] = { 0,0,0 }, R_TEME[3] = { 0,0,0 }, V_TEME[3] = { 0,0,0 };
	
	// extract LLA coordinates from GPS structure [rad, rad, m]
	double elong = double(gps_current->lon);
	double phi = double(gps_current->lat);
	double height = double(gps_current->alt);

	// calculate satellite's velocity [m/s] in LLA frame
	// V_East = V_tan * cos(azim)
	// V_North = V_tan * sin(azim)
	// V_Up = V_rad
	V_WGS84[0] = double(gps_current->vel) * cos(double(gps_current->azim));
	V_WGS84[1] = double(gps_current->vel) * sin(double(gps_current->azim));
	V_WGS84[2] = double(gps_current->lift);

	// calculate R and V in ECEF frame (R_ITRS and V_ITRS)
	WGS842ITRS_R(elong, phi, height, R_ITRS);
	WGS842ITRS_V(V_WGS84, elong, phi, V_ITRS);

	// calculate current Julian day in UTC
	// 2440587.5 -- Julian day of 0h Jun 1, 1970
	// gps_mark -- seconds, starting from 0h Jun 1, 1970 without leap seconds
	// 37 -- number of leap seconds (relevant for Jul 2021)
	double JD_UTC = 2440587.5 + gps_current->gps_mark / DAYSEC + 37 / DAYSEC;

	// calculate R and V in TEME frame (R_TEME and V_TEME)
	ITRS2TEME_RV_from_JD(JD_UTC, R_ITRS, V_ITRS, R_TEME, V_TEME);

	// ---- block for verifying the correctness of GPS data -----------------------------
	
	// if current day is not greater than NORAD + 1 day, do not update the current TLE and return 1
	if (JD_UTC < JD_NORAD + 1)
	{
		return 1;
	}
	// calculate the difference between dates in minutes
	double tsince = (JD_UTC - JD_NORAD) * 24 * 60;
	// propagate the NORAD TLE to the current date and calculate R and V in TEME frame (R_TEME_NORAD and V_TEME_NORAD) in km
	double R_TEME_NORAD[3] = { 0,0,0 }, V_TEME_NORAD[3] = { 0,0,0 };
	SGP4Funcs::sgp4(SATREC_NORAD, tsince, R_TEME_NORAD, V_TEME_NORAD);
	
	// convert R and V from GPS data from m(/s) to km(/s) (R_TEME_km and V_TEME_km)
	// R_TEME_km = R_TEME / 1000
	// V_TEME_km = V_TEME / 1000
	double R_TEME_km[3] = { 0,0,0 }, V_TEME_km[3] = { 0,0,0 }, R_diff[3] = { 0,0,0 };
	iauSxp(0.001, R_TEME, R_TEME_km);
	iauSxp(0.001, V_TEME, V_TEME_km);
	// find the difference between R_TEME vectors from GPS and NORAD (R_diff)
	// R_diff = R_TEME_km - R_TEME_NORAD
	iauPmp(R_TEME_km, R_TEME_NORAD, R_diff);

	// if the absolute value of difference is greater than 5 km, do not update the current TLE and return 2
	if (iauPm(R_diff) > 5)
	{
		return 2;
	}
	
	// convert R and V in TEME frame [km] into TLE
	char tle1[130], tle2[130];
	std::strncpy(tle1, TLE1_CURRENT, sizeof(tle1) - 1);
	tle1[sizeof(tle1) - 1] = 0;
	std::strncpy(tle2, TLE2_CURRENT, sizeof(tle2) - 1);
	tle2[sizeof(tle2) - 1] = 0;

	rv2el(R_TEME_km, V_TEME_km, tle1, tle2);

	// update global variables of current TLE
	std::strncpy(TLE1_CURRENT, tle1, sizeof(TLE1_CURRENT) - 1);
	TLE1_CURRENT[sizeof(TLE1_CURRENT) - 1] = 0;
	std::strncpy(TLE2_CURRENT, tle2, sizeof(TLE2_CURRENT) - 1);
	TLE2_CURRENT[sizeof(TLE2_CURRENT) - 1] = 0;

	return 0;
}

// propagate TLE_current to the time of JD_UTC and give RV in required reference frames
// inputs:
//		rf -- required reference frame, entry from RefFrame enumeration
//		JD_UTC -- current Julian date in UTC, positive double
// outputs:
//		R -- [m] satellite's position in reference frame rf (in case of LLA: R = { elong, lat_gd, height_gd }, V = { V_East, V_North, V_Up }), doubles
//		V -- [m/s] satellite's velocity in reference frame rf, doubles
void get_RV(RefFrame rf, double JD_UTC, double R[3], double V[3])
{
	// extract parameters from current TLE and put them into sgp4 structure (satrec)
	elsetrec satrec;
	SGP4Funcs::sgp4_init_TLE(TLE1_CURRENT, TLE2_CURRENT, satrec);

	// calculate Julian day, corresponding to current TLE
	double JD_TLE = satrec.jdsatepoch + satrec.jdsatepochF;
	// calculate the difference between dates (required and current) in minutes
	double tsince = (JD_UTC - JD_TLE) * 24 * 60;

	// update structure of satellite parameters (satrec), based on current TLE
	// 2433281.5 -- Julian day of 0h December 31, 1949
	SGP4Funcs::sgp4init(whichconst, 'i', satrec.satnum, JD_TLE - 2433281.5, satrec.bstar,
		satrec.ndot, satrec.nddot, satrec.ecco, satrec.argpo, satrec.inclo, satrec.mo, satrec.no_kozai,
		satrec.nodeo, satrec);

	double R_TEME[3] = { 0,0,0 }, V_TEME[3] = { 0,0,0 }, R_ITRS[3] = { 0,0,0 }, V_ITRS[3] = { 0,0,0 };
	double R_J2000[3] = { 0,0,0 }, V_J2000[3] = { 0,0,0 }, elong = 0, phi = 0, height = 0;
	double R_TEME_km[3] = { 0,0,0 }, V_TEME_km[3] = { 0,0,0 };

	// propagate the current TLE to the required date and calculate R and V in TEME frame (R_TEME_km and V_TEME_km) in km
	SGP4Funcs::sgp4(satrec, tsince, R_TEME_km, V_TEME_km);

	// convert R and V from km(/s) to m(/s) (R_TEME and V_TEME)
	// R_TEME = R_TEME_km * 1000
	// V_TEME = V_TEME_km * 1000
	iauSxp(1000, R_TEME_km, R_TEME);
	iauSxp(1000, V_TEME_km, V_TEME);

	// return new R and V depending on the reference frame rf (all in SI units)
	switch (rf)
	{
		// if TEME, copy calculated TEME values to the output
		// R = R_TEME
		// V = V_TEME
		case TEME:	iauCp(R_TEME, R);
					iauCp(V_TEME, V);
					break;

		// if ECEF, convert R_TEME and V_TEME into ITRS frame (R and V)
		case ECEF:	TEME2ITRS_RV_from_JD(JD_UTC, R_TEME, V_TEME, R, V);
					break;

		// if ECI, convert R_TEME and V_TEME into ITRS frame (R_ITRS and V_ITRS)
		// after that convert R_ITRS and V_ITRS into J2000 frame (R and V)
		case ECI:	TEME2ITRS_RV_from_JD(JD_UTC, R_TEME, V_TEME, R_ITRS, V_ITRS);
					ITRS2J2000_RV_from_JD(JD_UTC, R_ITRS, V_ITRS, R, V);
					break;
		
		// if LLA, convert R_TEME and V_TEME into ITRS frame (R_ITRS and V_ITRS)
		// after that convert R_ITRS and V_ITRS into WGS84 geodetic frame (R = {lon, lat, alt} and V)
		case LLA:	TEME2ITRS_RV_from_JD(JD_UTC, R_TEME, V_TEME, R_ITRS, V_ITRS);
					ITRS2WGS84_R(R_ITRS, &elong, &phi, &height);
					R[0] = elong;
					R[1] = phi;
					R[2] = height;
					ITRS2WGS84_V(V_ITRS, elong, phi, V);
					break;

		// if LVLH, convert R_TEME and V_TEME into ITRS frame (R_ITRS and V_ITRS)
		// after that convert R_ITRS and V_ITRS into J2000 frame (R_J2000 and V_J2000)
		// after that convert R_J2000 and V_J2000 into LVLH frame (R and V)
		case LVLH:	TEME2ITRS_RV_from_JD(JD_UTC, R_TEME, V_TEME, R_ITRS, V_ITRS);
					ITRS2J2000_RV_from_JD(JD_UTC, R_ITRS, V_ITRS, R_J2000, V_J2000);
					J20002LVLH_RV(R_J2000, V_J2000, R, V);
					break;
	}
}

int main(int argc, const char** argv)
{
	char tle1[] = "1 23177U 94040C   06175.45752052  .00000386  00000-0  76590-3 0    95";
	char tle2[] = "2 23177   7.0496 179.8238 7258491 296.0482   8.3061  2.25906668 97438";

	update_TLE_from_NORAD(tle1, tle2);

	gps_data_time_s gps;
	gps.alt = 205.439285;
	gps.azim = 5.02540922;
	gps.gps_mark = 1626441069;
	gps.lat = 0.971886277;
	gps.lon = 0.651725471;
	gps.stat = 0;
	gps.vel = 0;
	gps.lift = 0;

	int flag = 0;
	flag = update_TLE_from_GPS(&gps);

	double mf_SEU[3] = { 0,0,0 };
	magn_field_SEU(2458940.1933954097, 6371200, DPI/3, DPI/2, mf_SEU);

	std::cout << "Magn Field:\t" << mf_SEU[0] << "\t" << mf_SEU[1] << "\t" << mf_SEU[2] << "\n";

	return 0;
}