#pragma once

#include "orb_det.h"

/* mean equatorial radius of the Earth "a" in meters (for IGRF) */
#define MEAN_EQUAT_RAD (6371200)

void magn_field_SEU(double JD_UTC, double rad_gc, double colat_gc, double elong, double mf_SEU[3]);
void magn_field_ITRS(double JD_UTC, double r_ITRS[3], double mf_ITRS[3]);
void magn_field_J2000(double JD_UTC, double r_J2000[3], double mf_J2000[3]);