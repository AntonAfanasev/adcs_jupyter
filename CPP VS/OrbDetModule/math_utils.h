#pragma once

#include <math.h>
#include "sofa.h"
#include "sofam.h"
#include "config.h"

void DCM2QUAT(double DCM[3][3], double quat[4]);
void QUAT2DCM(double quat[4], double DCM[3][3]);

double acose(double x);
double angle(double vec1[3], double vec2[3]);
double fmod2p(const double x);