/*
This file contains miscellaneous functions required for coordinate transformation.
Functions were originally written for Matlab as companion code for "Fundamentals of Astrodynamics 
and Applications" by David Vallado (2007). (w) 719-573-2600, email dvallado@agi.com
Ported to C++ by Grady Hillhouse with some modifications, July 2015.
*/

#include "orb_det.h"
#include <cstring>

#define _SCL_SECURE_NO_WARNINGS
#pragma warning(disable:4996)

static const double
nocon = D2PI / 1440.,
two_thirds = 2. / 3.,
radiusearthkm = SMA_WGS84 / 1000, // in km
xke = 60.0 / sqrt(radiusearthkm * radiusearthkm * radiusearthkm / MU_WGS84);

/*
teme2ecef
This function transforms a vector from a true equator mean equinox (TEME)
frame to an earth-centered, earth-fixed (ECEF) frame.
Author: David Vallado, 2007
Ported to C++ by Grady Hillhouse with some modifications, July 2015.
INPUTS          DESCRIPTION                     RANGE/UNITS
rteme           Position vector (TEME)          km
vteme           Velocity vector (TEME)          km/s
jdut1           Julian date                     days
OUTPUTS         DESCRIPTION                     RANGE/UNITS
recef           Position vector (ECEF)          km
vecef           Velocity vector (ECEF)          km/s
*/


// vectors to SGP4 mean elements
void rvel(double* rr2, double* vv2, double* result)
{
    double xincl, xnodeo, eo, omegao, xmo, xno, _;
    double ck2 = 5.413079E-4;
    double xj3 = -2.53881E-6;
    int i;

    /* classical osculating orbit elements calculated from vectors rr2, vv2  */
    double xinck, xnodek, ek, wk, xn, rk, uk, aodp, pl, rdotk, rfdotk, temp;
    double h[3], n[3], vec[3], vk[3];

    iauSxp(1. / xke, vv2, vk);
    iauPxp(rr2, vk, h);
    pl = iauPdp(h, h);
    double vz[] = { 0, 0, 1 };
    double vy[3];
    iauPxp(vz, h, n);
    if ((abs(n[0]) < VERYSMALL) && (abs(n[1]) < VERYSMALL)) n[0] = 1.;
    iauPn(n, &_, n);
    rk = iauPm(rr2);
    rdotk = iauPdp(rr2, vv2) / rk;
    rfdotk = iauPm(h) * xke / rk;
    temp = iauPdp(rr2, n) / rk;
    uk = acose(temp);
    if (rr2[2] < 0.) uk = D2PI - uk;
    iauPxp(vk, h, vz);
    iauSxp(-1. / rk, rr2, vy);
    iauPpp(vz, vy, vec);
    ek = iauPm(vec);
    if (ek >= 1.) return;         // open orbit
    xnodek = atan2(n[1], n[0]);
    if (xnodek < 0.)
        xnodek += D2PI;
    temp = sqrt(h[0] * h[0] + h[1] * h[1]);
    xinck = atan2(temp, h[2]);
    temp = iauPdp(vec, n) / ek;
    wk = acose(temp);
    if (vec[2] < 0.)
        wk = fmod2p(D2PI - wk);
    aodp = pl / (1. - ek * ek);
    xn = xke * pow(aodp, -1.5);

    double cosio, sinio, sin2u, cos2u, temp1, temp2,
        rdot, rfdot, theta2, betal, x3thm1, x1mth2, x7thm1,
        esine, ecose, elsq, cosepw, sinepw, axn, ayn,
        cosu, sinu, capu, a3ovk2, xlcof, aycof, aynl, xll,
        xl, a0, a1, a2, d0, d1, beta, beta2, r, u;

    /*
    In the first loop the osculating elements rk, uk, xnodek, xinck, rdotk,
    and rfdotk are used as anchors to find the corresponding final SGP4
    mean elements r, u, xnodeo, xincl, rdot, and rfdot.  Several other final
    mean values based on these are also found: betal, cosio, sinio, theta2,
    cos2u, sin2u, x3thm1, x7thm1, x1mth2.  In addition, the osculating values
    initially held by aodp, pl, and xn are replaced by intermediate
    (not osculating and not mean) values used by SGP4.  The loop converges
    on the value of pl in about four iterations.
    */

    /*  seed value for first loop */
    xincl = xinck;
    u = uk;

    for (i = 0; i < 99; ++i)
    {
        a2 = pl;
        betal = sqrt(pl / aodp);
        temp1 = ck2 / pl;
        temp2 = temp1 / pl;
        cosio = cos(xincl);
        sinio = sin(xincl);
        sin2u = sin(2. * u);
        cos2u = cos(2. * u);
        theta2 = cosio * cosio;
        x3thm1 = 3. * theta2 - 1.;
        x1mth2 = 1. - theta2;
        x7thm1 = 7. * theta2 - 1.;
        r = (rk - .5 * temp1 * x1mth2 * cos2u)
            / (1. - 1.5 * temp2 * betal * x3thm1);
        u = uk + .25 * temp2 * x7thm1 * sin2u;
        xnodeo = xnodek - 1.5 * temp2 * cosio * sin2u;
        xincl = xinck - 1.5 * temp2 * cosio * sinio * cos2u;
        rdot = rdotk + xn * temp1 * x1mth2 * sin2u;
        rfdot = rfdotk - xn * temp1 * (x1mth2 * cos2u + 1.5 * x3thm1);
        temp = r * rfdot / xke;
        pl = temp * temp;

        // vis-viva equation
        temp = 2. / r - (rdot * rdot + rfdot * rfdot) / (xke * xke);
        aodp = 1. / temp;

        xn = xke * pow(aodp, -1.5);
        if (fabs(a2 - pl) < VERYSMALL) break;
    }

    /*
    The next values are calculated from constants and a combination of mean
    and intermediate quantities from the first loop.  These values all remain
    fixed and are used in the second loop.
    */


    // preliminary values for the second loop
    ecose = 1. - r / aodp;
    esine = r * rdot / (xke * sqrt(aodp));   /* needed for Kepler's eqn.  */
    elsq = 1. - pl / aodp;               /* intermediate eccentricity squared */
    a3ovk2 = -xj3 / ck2;
    xlcof = .125 * a3ovk2 * sinio * (3. + 5. * cosio)
        / (1. + cosio);
    aycof = .25 * a3ovk2 * sinio;
    temp1 = esine / (1. + sqrt(1. - elsq));
    cosu = cos(u);
    sinu = sin(u);

    /*
    The second loop normally converges in about six iterations to the final
    mean value for the eccentricity, eo.  The mean perigee, omegao, is also
    determined.  Cosepw and sinepw are found to twelve decimal places and
    are used to calculate an intermediate value for the eccentric anomaly,
    temp2.  Temp2 is then used in Kepler's equation to find an intermediate
    value for the true longitude, capu.
    */
    /*  seed values for loop  */
    eo = sqrt(elsq);
    omegao = wk;
    axn = eo * cos(omegao);

    for (i = 0; i < 99; ++i)
    {
        a2 = eo;
        beta = 1. - eo * eo;
        temp = 1. / (aodp * beta);
        aynl = temp * aycof;
        ayn = eo * sin(omegao) + aynl;
        cosepw = r * cosu / aodp + axn - ayn * temp1;
        sinepw = r * sinu / aodp + ayn + axn * temp1;
        axn = cosepw * ecose + sinepw * esine;
        ayn = sinepw * ecose - cosepw * esine;
        omegao = fmod2p(atan2(ayn - aynl, axn));
        //eo = axn / cos(omegao);
        eo = .9 * eo + .1 * (axn / cos(omegao));
        if (eo > .999) eo = .999;
        if (fabs(a2 - eo) < SMALL) break;
    }

    temp2 = atan2(sinepw, cosepw);
    capu = temp2 - esine;             /* Kepler's equation */
    xll = temp * xlcof * axn;

    /* xll adjusts the intermediate true longitude,  */
    /* capu, to the mean true longitude, xl          */
    xl = capu - xll;

    xmo = fmod2p(xl - omegao);        /* mean anomaly */

/*
The third loop usually converges after three iterations to the
mean semi-major axis, a1, which is then used to find the mean motion, xno.
*/

    a0 = aodp;
    a1 = a0;
    beta2 = sqrt(beta);
    temp = 1.5 * ck2 * x3thm1 / (beta * beta2);
    for (i = 0; i < 99; ++i)
    {
        a2 = a1;
        d0 = temp / (a0 * a0);
        a0 = aodp * (1. - d0);
        d1 = temp / (a1 * a1);
        a1 = a0 / (1. - d1 / 3. - d1 * d1 - 134. * d1 * d1 * d1 / 81.);
        if (fabs(a2 - a1) < VERYSMALL) break;
    }
    xno = xke * pow(a1, -1.5);

    result[0] = xincl;
    result[1] = xnodeo;
    result[2] = eo;
    result[3] = omegao;
    result[4] = xmo;
    result[5] = xno;

    return;
} /* end rvel  */

void update_tle(double* result, char* tle1, char* tle2, elsetrec satrec)
{
    //char tle1_updated[130];
    //char tle2_updated[130];

    double xincl = result[0];
    double xnodeo = result[1];
    double eo = result[2];
    double omegao = result[3];
    double xmo = result[4];
    double xno = result[5];

    //char line1[80], line2[80];
    char ec_string[10];

    snprintf(ec_string, sizeof(ec_string), "%.7f", eo);
    ec_string[0] = ec_string[2];
    ec_string[1] = ec_string[3];
    ec_string[2] = ec_string[4];
    ec_string[3] = ec_string[5];
    ec_string[4] = ec_string[6];
    ec_string[5] = ec_string[7];
    ec_string[6] = ec_string[8];


    //tle1_1 = tle1[0:18]+f[2:4]+('%3.8f' %(s.days+s.seconds/86400+1))+tle1[32:]
    //sprintf(line1, "1 %05dU %-8s %014.8f %10s  00000-0 %6s%2s 0    00",ssn, desig, tle, xns_string, bstar_fract, bstar_exp);
    //ccksum(line1);
    int t1 = snprintf(tle1, 69, "%.17s %02d%3.8f %.36s", tle1, satrec.epochyr, satrec.epochdays, tle1 + 33);
    int t2 = snprintf(tle2, 69, "2 %05s %8.4lf %8.4lf %.7s %8.4lf %8.4lf %11.8lf%6d",
        satrec.satnum, xincl * DR2D, xnodeo * DR2D, ec_string, omegao * DR2D, xmo * DR2D, xno / nocon, satrec.revnum);
    //ccksum(line2);
}
 
// convert RV in TEME frame into corresponding TLE
// source: http://sat.belastro.net/satelliteorbitdetermination.com/RV2EL.txt
// inputs:
//		rr -- [km] satellite's position in TEME, doubles
//		vv -- [km/s] satellite's velocity in TEME, doubles
// outputs:
//		tle1 -- first elements line, chars with fixed structure
//		tle2 -- second elements line, chars with fixed structure
void rv2el(double* rr, double* vv, char* tle1, char* tle2)
{   
    double rr_radmin[3], vv_radmin[3];
    iauSxp(1/radiusearthkm, rr, rr_radmin);
    iauSxp((60 / radiusearthkm), vv, vv_radmin);
    elsetrec satrec;
    double result[6]; 
    double ik, ok, ek, wk, mk, nk;
    double iz, oz, ez, wz, mz, nz;
    double rr1[3], vv1[3];
    char opsmode = 'i';
    gravconsttype  whichconst = wgs84;

    SGP4Funcs::sgp4_init_TLE(tle1, tle2, satrec); // use TLE to initialize elsetrec object 
    result[0] = 0;
    rvel(rr_radmin, vv_radmin, result);              // SGP4 elements
    //std::cout << "result5: " << result[5] / nocon << '\n';
    if (result[5] / nocon < 6.4)
    {
        
        rr1[0] = rr[0];
        rr1[1] = rr[1];
        rr1[2] = rr[2];
        vv1[0] = vv[0];
        vv1[1] = vv[1];
        vv1[2] = vv[2];
        
        ik = result[0],
        ok = result[1],
        ek = result[2],
        wk = result[3],
        mk = result[4],
        nk = result[5];
        
        update_tle(result, tle1, tle2, satrec); // use result and satrec to update TLE
        //std::cout << "1:tle: \n" << tle1 << '\n' << tle2 << '\n';

        SGP4Funcs::sgp4_init_TLE(tle1, tle2, satrec); // use the updated tle to update satrec

        SGP4Funcs::sgp4init(whichconst, opsmode, satrec.satnum, satrec.jdsatepoch + satrec.jdsatepochF - JD_SGP4_REF, satrec.bstar,
            satrec.ndot, satrec.nddot, satrec.ecco, satrec.argpo, satrec.inclo, satrec.mo, satrec.no_kozai,
            satrec.nodeo, satrec); // initialize the propagator with updated satrec values
        SGP4Funcs::sgp4(satrec, 0, rr, vv); // use updated satrec to propagate for tsince in minutes to get r,v in km, km/s
        iauSxp(1 / radiusearthkm, rr, rr_radmin); // update r,v_radmin
        iauSxp((60 / radiusearthkm), vv, vv_radmin);

        rvel(rr_radmin, vv_radmin, result);           // SDP4 elements first correction

        result[0] = 2 * ik - result[0];
        result[1] = 2 * ok - result[1];
        result[2] = 2 * ek - result[2];
        result[3] = 2 * wk - result[3];
        result[4] = 2 * mk - result[4];
        result[5] = 2 * nk - result[5];

        // Loop ---->
        for (int i = 0; i < 2; i++)
        {
            // These elements are pretty close.  Save these
            // as the "pretty close" reference elements
            iz = result[0],
            oz = result[1],
            ez = result[2],
            wz = result[3],
            mz = result[4],
            nz = result[5];

            update_tle(result, tle1, tle2, satrec);
            //std::cout << "2:tle: \n" << tle1 << '\n' << tle2 << '\n';

            SGP4Funcs::sgp4_init_TLE(tle1, tle2, satrec);

            SGP4Funcs::sgp4init(whichconst, opsmode, satrec.satnum, satrec.jdsatepoch + satrec.jdsatepochF - JD_SGP4_REF, satrec.bstar,
                satrec.ndot, satrec.nddot, satrec.ecco, satrec.argpo, satrec.inclo, satrec.mo, satrec.no_kozai,
                satrec.nodeo, satrec);
            SGP4Funcs::sgp4(satrec, 0, rr, vv);
            iauSxp(1 / radiusearthkm, rr, rr_radmin);
            iauSxp((60 / radiusearthkm), vv, vv_radmin);

            rvel(rr_radmin, vv_radmin, result);           // SDP4 elements second correction

            // Correct the "pretty close" reference elements
            result[0] = iz - (result[0] - ik);
            result[1] = oz - (result[1] - ok);
            result[2] = ez - (result[2] - ek);
            result[3] = wz - (result[3] - wk);
            result[4] = mz - (result[4] - mk);
            result[5] = nz - (result[5] - nk);

        } //  <---- end Loop

        if (result[0] < 0.) result[0] *= -1.;
        result[0] = fmod2p(result[0]);
        result[1] = fmod2p(result[1]);
        result[3] = fmod2p(result[3]);
        result[4] = fmod2p(result[4]);

        
        rr[0] = rr1[0];
        rr[1] = rr1[1];
        rr[2] = rr1[2];
        vv[0] = vv1[0];
        vv[1] = vv1[1];
        vv[2] = vv1[2];
        
    }
} // end rv2el  



