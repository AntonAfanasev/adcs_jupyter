import numpy as np
from scipy.linalg import eigvals
from scipy.integrate import solve_ivp
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import pyplot as plt

from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Avant Garde']})
rc('text', usetex=True)

MU = 3.986e+14                                 # [m^3 / s^2] standard gravitational parameter of the Earth
MU_0 = 1.257e-6                                # [N / A^2] vacuum permeability
MU_e = 7.94e+22                                # [A * m^2] magnetic dipole moment of the Earth
R_e = 6371e+3                                  # [m] radius of the Earth

def set_orbital_parameters(altitude):
	
	R_orb = R_e + altitude
	omega_0 = np.sqrt(MU / R_orb**3.)
	B_0 = MU_e * MU_0 / (4 * np.pi * R_orb**3.)
	
	return omega_0, B_0

def Jacobian(u, i, Kw, Ka, B_magn, omega_orb, J_sat):
    
    B = B_orb(u, i)
    B01 = B[0] * B[1]
    B02 = B[0] * B[2]
    B12 = B[1] * B[2]
    B01sqr = B[0]**2. + B[1]**2.
    B02sqr = B[0]**2. + B[2]**2.
    B12sqr = B[1]**2. + B[2]**2.
    
    kw = Kw * B_magn**2. / omega_orb / J_sat
    ka = 2. * Ka * B_magn**2. / J_sat
    
    return np.array([[-kw[0] * B12sqr,
                     kw[0] * B01,
                     kw[0] * B02 - omega_orb * ((J_sat[0] - J_sat[1] + J_sat[2]) / J_sat[0]),
                     -ka[0] * B12sqr + 4. * omega_orb**2 * ((J_sat[2] - J_sat[1]) / J_sat[0]),
                     ka[0] * B02,
                     ka[0] * B01],
                    [kw[1] * B01,
                     -kw[1] * B02sqr,
                     kw[1] * B12,
                     ka[1] * B01,
                     ka[1] * B12,
                     -ka[1] * B02sqr + 3. * omega_orb**2 * ((J_sat[2] - J_sat[0]) / J_sat[1])],
                    [kw[2] * B02 + omega_orb * ((J_sat[0] - J_sat[1] + J_sat[2]) / J_sat[2]),
                     kw[2] * B12,
                     -kw[2] * B01sqr,
                     ka[2] * B02,
                     -ka[2] * B01sqr + omega_orb**2 * ((J_sat[0] - J_sat[1]) / J_sat[2]),
                     ka[2] * B12],
                    [1., 0., 0., 0., 0., 0.],
                    [0., 0., 1., 0., 0., 0.],
                    [0., 1., 0., 0., 0., 0.]]) / omega_orb

def monodromy_array(Kw, Ka, B_magn, omega_orb, J_sat, i):
    
    def fun(u, x):
        
        return Jacobian(u, i, Kw, Ka, B_magn, omega_orb, J_sat).dot(x.reshape((6,6))).flatten()
    
    flat_mdrm = solve_ivp(fun, (0, 2 * np.pi), np.identity(6).flatten()).y[:,-1]
    return flat_mdrm.reshape((6,6))

def coef_distribution(K, **kw):
    
    Kw, Ka = K
    
    return np.max(np.real(np.log(eigvals(monodromy_array(Kw, Ka, kw['B_magn'], kw['omega_orb'], kw['J_sat'], kw['incl'])))))
	
def B_orb(u, i):
        
        return np.array([np.cos(u)*np.sin(i), np.cos(i), -2.*np.sin(u)*np.sin(i)])
	
def plot_coefficients(N1, N2, dN, n, incl, altitude, J):
    
    omega_0, B_0 = set_orbital_parameters(altitude)
    
    x = np.linspace(N1, N1+dN, n)
    X = np.repeat([x], n, axis=0)
    y = np.linspace(N2+dN, N2, n)
    Y = np.transpose(np.repeat([y], n, axis=0))

    f_points = np.dstack((X, Y))

    Z = np.apply_along_axis(coef_distribution, -1, f_points, B_magn=B_0, omega_orb=omega_0, J_sat=J, incl=incl)
    i,j = np.where(Z == np.min(Z))

    fig, ax = plt.subplots(1, 1, figsize=(6, 6), dpi = 300)

    im = ax.imshow(Z, interpolation='spline36', cmap=cm.coolwarm, extent=[N1, N1+dN, N2, N2+dN])
    # ax.set_title('Coefficients: $k\'_\omega =~${0}, $k_a =~${1}'.format(int(f_points[i,j,0]), int(f_points[i,j,1])))
    ax.set_xlabel('$k\'_\omega$', fontsize=16)
    ax.set_ylabel('$k_s$', fontsize=16)
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    plt.savefig('{0}_{1}'.format(int(f_points[i,j,0]), int(f_points[i,j,1])) + '.pdf', format='pdf', bbox_inches='tight')
    plt.show()