#include <cmath>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <armadillo>

using namespace std;

double vec_norm(vector<double>& u)
{
    double accum = 0.;
    for (int i = 0; i < u.size(); ++i)
    {
        accum += u[i] * u[i];
    }
    return sqrt(accum);
}

double vec_dot(vector<double>& u, vector<double>& v)
{
    double accum = 0.;
    for (int i = 0; i < u.size(); ++i)
    {
        accum += u[i] * v[i];
    }
    return accum;
}

void add_to_vec(vector<double>& u, double a, vector<double>& v, vector<double>& out)
{
    for (int i = 0; i < u.size(); ++i)
    {
        out[i] = u[i] + a * v[i];
    }
}

int main()
{
    vector<double> u = {0., -1., 2.};
    vector<double> v = {0., 1., 1.};
    double dot_prod = vec_dot(u, v);
    cout << "Dot product: " << dot_prod << endl;
    double norm = vec_norm(u);
    cout << "Vector norm: " << norm << endl;
    vector<double> out = {0., 0., 0.};
    add_to_vec(u, 1., v, out);
    cout << "Sum of vectors: " << out[0] << ", " << out[1] << ", " << out[2] << endl;
    return 0;
}